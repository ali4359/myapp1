<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});




Route::get('/shop', 'productsController@index');


Route::get('/product', function () {
    return view('product');
});

Route::get('/nero', function () {
    return view('nero');
});

Route::get('/harrier', function () {
    return view('harrier');
});

Route::get('/contactus', function () {
    return view('contact_process');
});

Route::get('/aura', function () {
    return view('Aura');
});

Route::get('/checkout', function () {
    return view('checkout');
});




Route::get('/novatech', function () {
    return view('novatech');
});

Route::get('/singleproduct', function () {
    return view('single-product');
});


Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');

Route::post('/login/admin', 'Auth\LoginController@adminLogin');

Route::get('/admin', function () {
    return view('AdminPanel/Dashboard');
});

Route::get('/admin/user', function () {
    return view('AdminPanel/user');
});

Route::get('/admin/tables', function () {
    return view('AdminPanel/tables');
});

Route::get('/admin/notifications', function () {
    return view('AdminPanel/notifications');
});

Route::get('/admin/addProducts', 'productsController@create');


Route::post('/admin/submit', 'productsController@save');









Auth::routes();

Route::get('/addToCart', 'cartController@add');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/viewcart', 'cartController@cart');

