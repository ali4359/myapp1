$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
	autoplay:true,
	autoplayTimeout:2000,
	navigation:true,
	navigationText: ["◀ Left <strong>arrow</strong>","Right <strong>arrow</strong> ▶"],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})

function s1(){
    document.getElementById("img1").src ="../img/category/grapling-glove-hover.jpg";
}

function s11(){
    document.getElementById("img1").src ="../img/category/grappling-glove.jpg";
}

function s2(){
    document.getElementById("img2").src ="../img/category/mma-glove-hover.jpg";
}

function s12(){
    document.getElementById("img2").src ="../img/category/mma-glove.jpg";
}

function s3(){
    document.getElementById("img3").src ="../img/category/boxing-glove-hover.jpg";
}

function s13(){
    document.getElementById("img3").src ="../img/category/boxing-glove.jpg";
}

function s4(){
    document.getElementById("img4").src ="../img/category/head-guard-hover.jpg";
}

function s14(){
    document.getElementById("img4").src ="../img/category/head-guard-new.jpg";
}

function s5(){
    document.getElementById("img5").src ="../img/category/shin-pad-hover.jpg";
}

function s15(){
    document.getElementById("img5").src ="../img/category/shin-guard.jpg";
}

function s6(){
    document.getElementById("img6").src ="../img/category/groin-guard-hover.jpg";
}

function s16(){
    document.getElementById("img6").src ="../img/category/groin-guards.jpg";
}

function s7(){
    document.getElementById("img7").src ="../img/category/hand-wrap-hover.jpg";
}

function s17(){
    document.getElementById("img7").src ="../img/category/hand-wraps.jpg";
}

function s8(){
    document.getElementById("img8").src ="../img/category/focus-pad-hover.jpg";
}

function s18(){
    document.getElementById("img8").src ="../img/category/focus-pads.jpg";
}
function s9(){
    document.getElementById("img9").src ="../img/category/arm-pad-hover.jpg";
}

function s19(){
    document.getElementById("img9").src ="../img/category/arm-pads.jpg";
}
