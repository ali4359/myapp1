<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use Illuminate\Support\Facades\DB;

class cartController extends Controller
{

    public function cart(){


        $data =  Cart::getContent();
        
        return view('viewcart')->with('products', $data);


    }


    public function add(Request $req){

        $id = (int) $req->input('p_id');
        
        $product = DB::table('products')->where('id', $id)->first();
        
        

        $add = Cart::add([
            'id' => $product->id,
            'name' => $product->P_Name,
            'price' => $product->Price,
            'quantity' => 1
            

        ]);

        if($add){
            echo "done";
        }
    }
}
