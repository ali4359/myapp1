<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Products;
use App\inventory_items;

class productsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {

        $id = $req->input('p_id');
        $products = DB::select("select * from products where foriegn_id = '$id'");
        return view('Shop')->with('products', $products);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $inventoryItems = inventory_items::all();
        return view('AdminPanel/Products')->with('Inventory', $inventoryItems);
    }


    public function save(Request $req){



        $p = new Products;

        $p->P_name = $req->p_name;
        $p->Category = $req->p_category;
        $p->Sub_Category = $req->p_sub_category;
        $p->Price = $req->price;
        $p->Description = $req->description;
        
        
        if($req->hasFile('p_photo')){

            $filenamewithext = $req->file('p_photo')->getClientOriginalName();
            
            $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);

            $extension = $req->file('p_photo')->getClientOriginalExtension();

            $fileNameToStore = $filename.''.time().'.'.$extension;
            
            $path = $req->file('p_photo')->storeAs('public/image', $fileNameToStore);
        }
        
        
        $p->productPhoto = $fileNameToStore;
        $foriegn_id_record = DB::table('inventory_items')->where('Product_Category', $req->p_name)->first();
        $p->foriegn_id = $foriegn_id_record->id;
        $p->Old_Price = $req->old_price;
        $p->save();

        return view('AdminPanel/submit');

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
