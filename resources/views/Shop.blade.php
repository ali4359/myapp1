<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="CodePixar">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>Karma Shop</title>
	<!--
		CSS
        ============================================= -->
        
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> 
	<link rel="stylesheet" href="css/linearicons.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/themify-icons.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/nice-select.css">
	<link rel="stylesheet" href="css/nouislider.min.css">
	<link rel="stylesheet" href="css/ion.rangeSlider.css" />
	<link rel="stylesheet" href="css/ion.rangeSlider.skinFlat.css" />
	<link rel="stylesheet" href="css/magnific-popup.css">
	<link rel="stylesheet" href="css/main.css">
	
	<link rel="stylesheet" href="css/style.css">
    
    <link rel="stylesheet" href="css/productpage.css">
    <link href="assets/owl.carousel.min.css" rel="stylesheet"/>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/style3.css">
<script src="/js/tog.js"></script>



<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="/js/amazonmenu.js"></script>
<link rel="stylesheet" href="css/amazonmenu.css">
<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu1'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu2'
		
	})
})

</script>



<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu3'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu4'
		
	})
})

</script>






<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>
@include('layout.menubar')
<div class="jumbotron jumbotron-fluid" style="background-image:url('/img/j3.jpg');background-repeat: no-repeat, repeat;background-size: cover;margin-top:100px">
  <div class="container">
    <h1 class="display-4" style="color:whitesmoke;text-align:center">Our Shop</h1>
  </div>
</div>

<ul class="breadcrumb" style="margin:-20px 0px 50px 0px">
  <li><a href="/">Home</a></li>
  <li class="active">Shop</li>
</ul>



<div class="container" style="margin-bottom:10px">
    <div class="row">
        <div class="col-md-2" style="background-color:white">
            <h4 style="background-color:orange;padding:10px"><b style="color:black">Shop by Categories<span class="caret" style="color:black"></span></b></h4>
            <ul>
            <li><a> Angle & Uppercut Bags</a></li>
            <li><a> Training Bags With Mitts</a></li>
           <li><a>Accessories</a></li>
            <li><a>Punch Bag With Gloves</a></li>
            <li><a>Maize Bags</a></li>
            <li><a>Accessories</a> </li>
            <li><a>Punch Bag With Gloves</a></li>
            <li><a>Maize Bags</a></li>
</ul>
<hr>

<hr>
           
            <div class="header-item" >Colors</div>
            <ul class="color-row1">
                <li class="color-circle" style="background:#4286f4"></li>
                <li class="color-circle" style="background:#2acc4b"></li>
                <li class="color-circle" style="background:#343534"></li>
                <li class="color-circle" style="background:#5f605f"></li>
                <li class="color-circle" style="background:#929392"></li>
            </ul>
            <ul class="color-row2">
                <li class="color-circle" style="background:#9e8045"></li>
                <li class="color-circle" style="background:#d3d3d3"></li>
                <li class="color-circle" style="background:#6b6666"></li>
                <li class="color-circle" style="background:#999797"></li>
                <li class="color-circle" style="background:#923476"></li>
            </ul>
            <hr>

<hr>
        
            <div class="header-item" >Size</div>
            <ul class="color-row1">
                <li class="color-circle size-circle" ><p class="sizedouble" style="padding-left:6px">XS</p></li>
                <li class="color-circle size-circle" style="background-color:#343534" ><p style="color:#999;padding-left:10px" class="size">S</p></li>
                <li class="color-circle size-circle" ><p class="size" style="padding-left:6px">M</p></li>
                <li class="color-circle size-circle" ><p class="size" style="padding-left:6px">L</p></li>
                <li class="color-circle size-circle" ><p class="sizedouble" style="padding-left:6px">XL</p></li>
            </ul>
        
</div>



<div class="col-md-10">
<div class="single-product-slider">
			<div class="container">
				<div class="row">
					<!-- single product -->
					@if(count($products) > 0)
						@foreach($products as $product)
							<div class="col-lg-3 col-md-6">
								<div class="single-product" style="background-color:grey ;padding:10px">
									<img class="img-fluid" src="/storage/image/{{$product->productPhoto}}" alt="">
									<div class="product-details">
										<h6 style="color:white">{{$product->P_Name}}
										{{$product->Description}}</h6>
										<div class="price">
											<h6 style="color:white">${{$product->Price}}</h6>
											<h6 class="l-through">${{$product->Old_Price}}</h6>
										</div>
										<div class="prd-bottom">

											<a href="/addToCart?p_id={{$product->id}}" class="social-info">
												<span class="ti-bag"></span>
												<p class="hover-text" style="color:white">add to cart</p>
											</a>
									
											<a href="" class="social-info">
												<span class="lnr lnr-move"></span>
												<p class="hover-text" style="color:white">view more</p>
											</a>
										</div>
									</div>
								</div>
							</div>
							<!-- single product -->
						@endforeach	
  					@else
					  <h2> No Products has been added yet </h2>	
					@endif
				</div>
			</div>
		</div>
</div>
</div>
</div>



@include('layout.Footer')

	<script src="js/vendor/jquery-2.2.4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	 crossorigin="anonymous"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/jquery.nice-select.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/nouislider.min.js"></script>
	<script src="js/countdown.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
	<script src="instafeed.min.js"></script>
	<script src="custom.js"></script>
	
    
    
  
   
	<!--gmaps Js-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="js/gmaps.min.js"></script>
    <script src="js/main.js"></script>
    
   
</body>

</html>