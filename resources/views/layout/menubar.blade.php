	<!-- mega menu -->
	<nav class="navbar navbar-inverse sticky-header" style="background-image:linear-gradient(to right, rgba(255,186,0,1), rgba(255,108,0,1)); z-index:4" >
    <div class="navbar-header" style="">
    	<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<img class="nav-brand" src="/img/logo3.png"></img>
	</div>
	
	<div class="collapse navbar-collapse js-navbar-collapse">
		<ul class="nav navbar-nav" style="">
			<li class="dropdown mega-dropdown">
				<a href="/product" class="dropdown-toggle" data-toggle="dropdown" style="color:black"><b style="color:black;font-size:16px">Boxing</b></a>				
				<ul class="dropdown-menu mega-dropdown-menu">
					<li class="col-sm-3">
						<ul>
            <li class="dropdown-header"> 
            <nav id="mysidebarmenu" class="amazonmenu">
<ul>

  <!-- New Dropdown -->

	
	<li><a href="#">Boxing Gloves</a>
	  <ul>
<span class="a2">
    <li><a href="/shop?p_id=1">Competition Gloves</a></li>
                            <li><a href="/shop?p_id=2">Sparring Gloves</a></li>
                            <li><a href="/shop?p_id=3">Training Gloves</a></li>
              
              <li><a href="/shop?p_id=4">Knee Wraps</a></li>
              <li><a href="/shop?p_id=5">Anklet Sleeves</a></li>
              <li><a href="/shop?p_id=6">Kids Protective Gear</a></li>
</span>

	  </ul>
  </li>

<hr>
  	
	<li><a href="#">Punch Bags</a>
	  <ul>
    <span class="a2">  
    <li><a href="/shop?p_id=7">Training Bags</a></li>
                            <li><a href="/shop?p_id=8">Training Bags with Mitts</a></li>
                            <li><a href="/shop?p_id=9">Punch bag with Gloves</a></li>
              
              <li><a href="/shop?p_id=10">Punch Bag Sets</a></li>
              <li><a href="/shop?p_id=11">Kids Punch Bags</a></li>
              <li><a href="/shop?p_id=12">Maize Bags</a></li>
              <li><a href="/shop?p_id=13">Standing Punch Bags</a></li>
              <li><a href="/shop?p_id=14">Angle & Uppercut Bags</a></li>
              <li><a href="/shop?p_id=15">Double End Bags</a></li>
              <li><a href="/shop?p_id=16">Speed Bags</a></li>
              <li><a href="/shop?p_id=17">Accessories</a></li>
    
</span>
	  </ul>
  </li>
    <hr>
  	
	<li><a href="#">Coaching Equipment</a>
	  <ul>
<span class="a2">
    <li><a href="/shop?p_id=18">Focus Pads</a></li>
							<li><a href="/shop?p_id=19">Paddles Mitts</a></li>
							<li><a href="/shop?p_id=20">Training Sticks</a></li>
							<li><a href="/shop?p_id=21">Body Protectors</a></li>
    
</span>
	  </ul>
  </li>
    
  <hr>	
	<li><a href="#">Protecting Gear</a>
	  <ul>
      <span class="a2">
    <li><a href="/shop?p_id=22">Hand Wraps & Inner Gloves</a></li>
							<li><a href="/shop?p_id=23">HeadGear</a></li>
							<li><a href="/shop?p_id=24">MouthGuards</a></li>
              <li><a href="/shop?p_id=25">Chest Guards</a></li> 
</span>
	  </ul>
  </li>
    <hr>
  	
	<li><a href="#">Training Equipment  </a>
	  <ul>
    <span class="a2">  
    <li><a href="/shop?p_id=26">Jump Ropes</a></li>
    
							<li><a href="/shop?p_id=27">Pull Up Bags</a></li>
        
              <li><a href="/shop?p_id=28">Medicine Balls</a></li>	
</span>
	  </ul>
  </li>
  	<hr>
  	
	<li><a href="#">Apparel</a>
	  <ul>
    <span class="a2">
    <li><a href="/shop?p_id=29">Boxing Trunks</a></li>
    
        
    <li><a href="/shop?p_id=30">Compression Wear</a></li>
              <li><a href="/shop?p_id=31">T-Shirts & Vests </a></li>
</span>       							
	  </ul>
  </li>
  

  


	</ul>
</nav>
            
            </li>   




						                       
                            
                            
						</ul>
					</li>
				
          
					<li class="col-sm-3">
						<ul>


            
						
						</ul>
          </li>
          
          


          <li class="col-sm-3">
						<ul>
              <li>
            <div id="menCollection" class="carousel slide" data-ride="carousel">
                              <div class="carousel-inner">
                                <div class="item active">
                                    <a href="#"><img src="/img/e2.jpg" class="img-responsive" alt="product 1"></a>
                                    <h4><small>Summer dress floral prints</small></h4>                                        
                                    <button class="btn btn-primary" type="button">49,99 €</button> <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>       
                                </div><!-- End Item -->
                                     
                              </div><!-- End Carousel Inner -->
                              <!-- Controls -->
                              
                            </div><!-- /.carousel -->
</li>
						</ul>
          </li>
          
         

          
          
			
				</ul>				
      </li>


      <li class="dropdown mega-dropdown">
				<a href="/product" class="dropdown-toggle" data-toggle="dropdown" style="color:black"><b style="color:black;font-size:16px">MMA</b></a>				
				<ul class="dropdown-menu mega-dropdown-menu">
					<li class="col-sm-3">
						<ul>
            <li class="dropdown-header"> 
            <nav id="mysidebarmenu1" class="amazonmenu">
	<ul>

  <!-- New Dropdown -->

	
	<li><a href="#">MMA Gloves</a>
	  <ul>
<span class="a2">
<li><a href="/shop?p_id=7">Training Bags</a></li>
                            <li><a href="/shop?p_id=8">Training Bags With Mitts</a></li>
                            <li><a href="/shop?p_id=9">Punch Bag</a></li>
							<li><a href="/shop?p_id=32">Kids Grappling Gloves</a></li>
</span>

	  </ul>
  </li>

<hr>
  	
	<li><a href="#">MMA Punch Bags</a>
	  <ul>
    <span class="a2">  
    
    <li><a href="/shop?p_id=8">Training Bags</a></li>
							<li><a href="/shop?p_id=9">Training Bags With Mitts</a></li>
						
              <li><a href="/shop?p_id=12">Maize Bags</a></li>		
              <li><a href="/shop?p_id=13">Standing Punch Bags</a></li>		
              <li><a href="/shop?p_id=17">Accessories</a></li>	
             
             
</span>
	  </ul>
  </li>
    <hr>
  	
	<li><a href="#">Coaching Equipment</a>
	  <ul>
<span class="a2">
   
            
    <li><a href="/shop?p_id=33">Focus Mitts </a></li>
              <li><a href="/shop?p_id=34">Kicking Shields</a></li>
              <li><a href="/shop?p_id=35">Thai Pads</a></li>
              <li><a href="/shop?p_id=25">Chest Guard</a></li>
    
</span>
	  </ul>
  </li>
    
  <hr>	
	<li><a href="#">Protecting Gear</a>
	  <ul>
      <span class="a2">
    
      <li><a href="/shop?p_id=36">Hand Wraps & Inner Gloves</a></li>
							<li><a href="/shop?p_id=23">HeadGear</a></li>
							<li><a href="/shop?p_id=24">MouthGuards</a></li>
              <li><a href="/shop?p_id=25">Chest Guards</a></li> 

</span>
	  </ul>
  </li>
    <hr>
  	
	<li><a href="#">Equipment Bags  </a>
	  <ul>
    <span class="a2">  
    <li><a href="/shop?p_id=6">Jump Ropes</a></li>
    
							<li><a href="/shop?p_id=27">Pull Up Bags</a></li>
        
              <li><a href="/shop?p_id=28">Medicine Balls</a></li>	
</span>
	  </ul>
  </li>
  	<hr>
  	
	<li><a href="#">Apparel</a>
	  <ul>
    <span class="a2">

    <li><a href="/shop?p_id=37">MMA Shorts</a></li>
              <li><a href="/shop?p_id=38">MMA Socks</a></li>
              <li><a href="/shop?p_id=39">Compression Wear</a></li>
              <li><a href="/shop?p_id=40">BJJ GIS </a></li>    
              <li><a href="/shop?p_id=41">BJJ Belts </a></li>  
   
</span>       							
	  </ul>
  </li>
  

  


	</ul>
</nav>
            
            </li>   




						                       
                            
                            
						</ul>
					</li>
				
          
					<li class="col-sm-3">
						<ul>


            
						
						</ul>
          </li>
          
          


          <li class="col-sm-3">
						<ul>
              <li>
            <div id="menCollection" class="carousel slide" data-ride="carousel">
                              <div class="carousel-inner">
                                <div class="item active">
                                    <a href="#"><img src="/img/e1.jpg" class="img-responsive" alt="product 1"></a>
                                    <h4><small>Summer dress floral prints</small></h4>                                        
                                    <button class="btn btn-primary" type="button">49,99 €</button> <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>       
                                </div><!-- End Item -->
                                     
                              </div><!-- End Carousel Inner -->
                              <!-- Controls -->
                              
                            </div><!-- /.carousel -->
</li>
						</ul>
          </li>
          
         

          
          
			
				</ul>				
      </li>

      
      <!--Fitness-->
      
      <li class="dropdown mega-dropdown">
				<a href="/product" class="dropdown-toggle" data-toggle="dropdown" style="color:black"><b style="color:black;font-size:16px">Fitness</b></a>				
				<ul class="dropdown-menu mega-dropdown-menu">
					<li class="col-sm-3">
						<ul>
            <li class="dropdown-header"> 
            <nav id="mysidebarmenu2" class="amazonmenu">
<ul>

  <!-- New Dropdown -->

	
	<li><a href="#">Gym Gloves</a>
	  <ul>
<span class="a2">
<li><a href="/shop?p_id=42">Fitness & Workout</a></li>
                            <li><a href="/shop?p_id=43">Training & Gym</a></li>
                            <li><a href="/shop?p_id=44">Heavy Weight Lifting</a></li>
							<li><a href="/shop?p_id=45">Weight Lifting Grips</a></li>
</span>

	  </ul>
  </li>

<hr>
  	
	<li><a href="#">Strength Training</a>
	  <ul>
    <span class="a2">  
    <li><a href="/shop?p_id=27">Pull Up Bags</a></li>
              <li><a href="/shop?p_id=26">Jumps Ropes</a></li>
              <li><a href="/shop?p_id=46">Leg Stretches</a></li>
              <li><a href="/shop?p_id=47">Fitness Bags</a></li>	
              <li><a href="/shop?p_id=28">Medicine Balls</a></li>	
              <li><a href="/shop?p_id=48">Weighted Vests</a></li>	
    
</span>
	  </ul>
  </li>
    <hr>
  	
	<li><a href="#">WeightLifting Belts</a>
	  <ul>
<span class="a2">
<li><a href="/shop?p_id=49">Leather Belts</a></li>
							<li><a href="/shop?p_id=50">Lycra Support Belts</a></li>
							<li><a href="/shop?p_id=51">Dipping Belts </a></li>                            
              <li><a href="/shop?p_id=52">Powerlifting Belts</a></li>		
              	
    
</span>
	  </ul>
  </li>
    
  <hr>	
	<li><a href="#">WeightLifting Gear</a>
	  <ul>
      <span class="a2">
      <li><a href="/shop?p_id=53">WeightLifting Grips & Straps</a></li>
              <li><a href="/shop?p_id=54">Arm Blaster</a></li>
              <li><a href="/shop?p_id=55">Ab Strap & Triceps Rope</a></li>
              <li><a href="/shop?p_id=56">Head Harness</a></li>
</span>
	  </ul>
  </li>
    <hr>
  	
	<li><a href="#">Neoprene Support</a>
	  <ul>
    <span class="a2">  
    <li><a href="/shop?p_id=57">Ankle Support</a></li>
							<li><a href="/shop?p_id=58">Elbow Support</a></li>
							<li><a href="/shop?p_id=59">Knee Suport</a></li>
              <li><a href="/shop?p_id=60">Wrist Support</a></li> 
              
              <li><a href="/shop?p_id=61">Shin & Calf Support</a></li> 
</span>
	  </ul>
  </li>
  	<hr>
  	
	<li><a href="#">Gym Essentials</a>
	  <ul>
    <span class="a2">
    <li><a href="/shop?p_id=62">Sauna Suits</a></li>
              <li><a href="/shop?p_id=63">Track Suits</a></li>
              <li><a href="/shop?p_id=30">Compression Wear</a></li>
              <li><a href="/shop?p_id=64">Equipment Bags</a></li>    
</span>       							
	  </ul>
  </li>
  

  


	</ul>
</nav>
            
            </li>   




						                       
                            
                            
						</ul>
					</li>
				
          
					<li class="col-sm-3">
						<ul>


            
						
						</ul>
          </li>
          
          


          <li class="col-sm-3">
						<ul>
              <li>
            <div id="menCollection" class="carousel slide" data-ride="carousel">
                              <div class="carousel-inner">
                                <div class="item active">
                                    <a href="#"><img src="/img/e3.jpg" class="img-responsive" alt="product 1"></a>
                                    <h4><small>Summer dress floral prints</small></h4>                                        
                                    <button class="btn btn-primary" type="button">49,99 €</button> <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>       
                                </div><!-- End Item -->
                                     
                              </div><!-- End Carousel Inner -->
                              <!-- Controls -->
                              
                            </div><!-- /.carousel -->
</li>
						</ul>
          </li>
          
         

          
          
			
				</ul>				
      </li>
 

      <!--Clothing new -->

      <li class="dropdown mega-dropdown">
				<a href="/product" class="dropdown-toggle" data-toggle="dropdown" style="color:black"><b style="color:black;font-size:16px">Clothing</b></a>				
				<ul class="dropdown-menu mega-dropdown-menu">
					<li class="col-sm-3">
						<ul>
            <li class="dropdown-header"> 
            <nav id="mysidebarmenu3" class="amazonmenu">
<ul>

  <!-- New Dropdown -->

	
	<li><a href="#">Shorts</a>
	  <ul>
<span class="a2">
<li><a href="/shop?p_id=37">MMA Shorts</a></li>
                            <li><a href="/shop?p_id=65">Muay Thai Shorts</a></li>
                            <li><a href="/shop?p_id=66">Boxing Shorts</a></li>
</span>

	  </ul>
  </li>

<hr>
  	
	<li><a href="#">Compression Wear</a>
	  <ul>
    <span class="a2">  
    <li><a href="/shop?p_id=67">Compression Suits</a></li>
              <li><a href="/shop?p_id=68">Compression Shorts & Pants</a></li>
              <li><a href="/shop?p_id=69">SweatShirts</a></li>
    
</span>
	  </ul>
  </li>
    <hr>
  	
	<li><a href="#">Hoodies & Jackets</a>
	  <ul>
<span class="a2">
<li><img src="/img/hj.jpg" width="250px">
              	
    
</span>
	  </ul>
  </li>
    
  <hr>	
	<li><a href="#">Trousers</a>
	  <ul>
      <span class="a2">
      <li><img src="/img/t.jpg" width="250px">
</span>
	  </ul>
  </li>
    <hr>
  	
	<li><a href="#">T-Shirts & Vests</a>
	  <ul>
    <span class="a2">  
    <li><img src="/img/tv.jpg" width="250px">
</span>
	  </ul>
  </li>
  	<hr>
  	
	<li><a href="#">BJJ</a>
	  <ul>
    <span class="a2">
    <li><a href="/shop?p_id=40">BJJ GIS</a></li>
							<li><a href="/shop?p_id=41">BJJ Belts</a></li>   
</span>       							
	  </ul>
  </li>
  

  


	</ul>
</nav>
            
            </li>   




						                       
                            
                            
						</ul>
					</li>
				
          
					<li class="col-sm-3">
						<ul>


            
						
						</ul>
          </li>
          
          


          <li class="col-sm-3">
						<ul>
              <li>
            <div id="menCollection" class="carousel slide" data-ride="carousel">
                              <div class="carousel-inner">
                                <div class="item active">
                                    <a href="#"><img src="/img/e4.jpg" class="img-responsive" alt="product 1"></a>
                                    <h4><small>Summer dress floral prints</small></h4>                                        
                                    <button class="btn btn-primary" type="button">49,99 €</button> <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>       
                                </div><!-- End Item -->
                                     
                              </div><!-- End Carousel Inner -->
                              <!-- Controls -->
                              
                            </div><!-- /.carousel -->
</li>
						</ul>
          </li>
          
         

          
          
			
				</ul>				
      </li>

     
   
    
      	<li class="dropdown mega-dropdown">
				<a href="/product" class="dropdown-toggle" data-toggle="dropdown" style="color:black"><b style="color:black;font-size:16px">Sports</b></a>				
				<ul class="dropdown-menu mega-dropdown-menu">
					<li class="col-sm-3">
						<ul>
            <li class="dropdown-header"> 
            <nav id="mysidebarmenu4" class="amazonmenu">
<ul>

  <!-- New Dropdown -->

	
	<li><a href="#">Boxing</a>
	  <ul>
<span class="a2">
<li><a href="/shop?p_id=70">Boxing Gloves</a></li>
                            <li><a href="/shop?p_id=66">Boxing Shorts</a></li>
                            <li><a href="/shop?p_id=24">Mouth Guards</a></li>
              
              <li><a href="/shop?p_id=71">Boxing Pads</a></li>
</span>

	  </ul>
  </li>

<hr>
  	
	<li><a href="#">MMA</a>
	  <ul>
    <span class="a2">  
    <li><a href="/shop?p_id=70">MMA Gloves</a></li>
              <li><a href="/shop?p_id=37">MMA Shorts</a></li>
             
              <li><a href="/shop?p_id=24">Mouth Guards</a></li>	
              <li><a href="/shop?p_id=7">Training Gear</a></li>	
    
</span>
	  </ul>
  </li>
    <hr>
  	
	<li><a href="#">Taekwondo</a>
	  <ul>
<span class="a2">
<li><a href="/shop?p_id=72">TKD Gloves</a></li>
							<li><a href="/shop?p_id=73">TKD Shin Guards</a></li>
							<li><a href="/shop?p_id=74">TKD Chest Guards </a></li>                            
              
             
    
</span>
	  </ul>
  </li>
    
  <hr>	
	<li><a href="#">Muay Thai</a>
	  <ul>
      <span class="a2">
      <li><a href="/shop?p_id=75">Muay Thai Gloves</a></li>
              <li><a href="/shop?p_id=65">Muay Thai Shorts</a></li>
             
              <li><a href="/shop?p_id=76">Hand Wraps</a></li>
              <li><a href="/shop?p_id=77">Elbow Sleeves</a></li>
</span>
	  </ul>
  </li>
    <hr>
  	
	<li><a href="#">JIU JITSU </a>
	  <ul>
    <span class="a2">  
    
    <li><a href="/shop?p_id=40">BIJ GIS</a></li>
              <li><a href="/shop?p_id=41">BIJ Belts </a></li>
                            
</span>
	  </ul>
  </li>
  	<hr>
  	
	<li><a href="#">Training</a>
	  <ul>
    <span class="a2">
    <li><a href="/shop?p_id=78">Pull Up Bar</a></li>
							<li><a href="/shop?p_id=79">Skipping Ropes</a></li>
						
              <li><a href="/shop?p_id=80">Shorts</a></li> 
              <li><a href="/shop?p_id=81">Trousers</a></li> 
              
             
</span>       							
	  </ul>
  </li>
<hr>

  

  


	</ul>
</nav>
            
            </li>   




						                       
                            
                            
						</ul>
					</li>
				
          
					<li class="col-sm-3">
						<ul>


            
						
						</ul>
          </li>
          
          


          <li class="col-sm-3">
						<ul>
              <li>
            <div id="menCollection" class="carousel slide" data-ride="carousel">
                              <div class="carousel-inner">
                                <div class="item active">
                                    <a href="#"><img src="/img/e4.jpg" class="img-responsive" alt="product 1"></a>
                                    <h4><small>WeightLifting</small></h4>                                        
                                    <button class="btn btn-primary" type="button">49,99 €</button> <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>       
                                </div><!-- End Item -->
                                     
                              </div><!-- End Carousel Inner -->
                              <!-- Controls -->
                              
                            </div><!-- /.carousel -->
</li>
						</ul>
          </li>
          
         

          
          
			
				</ul>				
      </li>






        <!--  <li><a href="/novatech" style="color:black"><b style="color:black;font-size:16px">Nova Tech</b></a></li> -->
            <li><a href="/aura" style="color:black"><b style="color:black;font-size:16px">Aura</b></a></li>
            <li><a href="/harrier" style="color:black"><b style="color:black;font-size:16px">Harrier</b></a></li>
            <li><a href="/nero" style="color:black"><b style="color:black;font-size:16px">Nero</b></a></li>
		</ul>
        <ul class="nav navbar-nav navbar-right">
        
        <li><a href="/viewcart" style="color:black">My cart (0) items</a></li>
        
        @guest
                     <li class="nav-item">
                                <a style="color:black" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>        
                            @if (Route::has('register'))
                                <li>
                                    <a style="color:black" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>    
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
      </ul>
      
	</div><!-- /.nav-collapse -->
  </nav>
	<!-- mega menu -->


