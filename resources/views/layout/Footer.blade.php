
	<!-- start footer Area -->
	<footer class="footer-area section_gap">
		<div class="container">
			<div class="row">
				<div class="col-lg-3  col-md-6 col-sm-6">
					<div class="single-footer-widget">
						
				
						<h6>About Us</h6>
						<a href="#" style="color:white">Boxing</a><br>
						<a href="#" style="color:white">MMA</a><br>
						<a href="#" style="color:white">Fitness</a><br>
						<a href="#" style="color:white">Clothing</a><br>
						<a href="#" style="color:white">Sports</a><br>
						<a href="#" style="color:white">Events</a>
						
					</div>
				</div>
				<div class="col-lg-4  col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h6>Media</h6>
						<a href="#" style="color:white">Coperate News</a><br>
						<a href="#" style="color:white">Fitness Tips</a><br>
						<a href="#" style="color:white">RDX TV</a><br>
						<a href="#" style="color:white">Brand Ambassadors</a><br>
						<a href="#" style="color:white">WholeSale Blog</a><br>
						<a href="#" style="color:white">Social Shop</a><br>
						<a href="#" style="color:white">CEO Message</a><br>
					</div>
				</div>
				<div class="col-lg-3  col-md-6 col-sm-6">
					<div class="single-footer-widget mail-chimp">
						<h6 class="mb-20">Company</h6>
						<a href="#" style="color:white">About us</a><br>
						<a href="#" style="color:white">Affliate Program</a><br>
						<a href="#" style="color:white">Wholesale</a><br>
						<a href="#" style="color:white">Trade Catalog</a><br>
						<a href="#" style="color:white">WholeSale</a><br>
						<a href="#" style="color:white">Technology</a><br>
						<a href="#" style="color:white">Contact us</a><br>
					</div>
				</div>
				<div class="col-lg-2 col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h6>Customer Help</h6>
						
						
						<a href="#" style="color:white">FAQ</a><br>
						<a href="#" style="color:white">Site Chart</a><br>
						<a href="#" style="color:white">RDX Authenticator</a><br>
						<a href="#" style="color:white">Shipment & Conditions</a><br>
						<a href="#" style="color:white">Terms & Conditions</a><br>
						<a href="#" style="color:white">Privacy Policy</a><br>
						<a href="#" style="color:white">Return Policy</a><br>
						
					</div>
				</div>
			</div>
			<div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
				
			</div>

			
				<hr style="background-color:red">

				<div class="row">
				<div class="col-lg-3  col-md-6 col-sm-6">
					<div class="single-footer-widget">
						
					
						<h4 style="color:white">Call Us +44 808 189 4444</h4>
						<p>© 1999-2020 RDX LLC. All rights reserved.</p>
					</div>
				</div>
				<div class="col-lg-4  col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h4 style="color:white; margin-left:2%">Excellent</h4><img src="https://i.ya-webdesign.com/images/5-stars-png-no-background-4.png" width="110" height="30">
						
					</div>
				</div>
				<div class="col-lg-3  col-md-6 col-sm-6">
					<div class="single-footer-widget mail-chimp">
					<img src="/img/cards.jpg" width="180" height="30">
						
					</div>
				</div>
				<div class="col-lg-2 col-md-6 col-sm-6">
					<div class="single-footer-widget">
					<h4 style="color:white">Let us be social</h4>
						<div class="footer-social d-flex align-items-center">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-dribbble"></i></a>
							<a href="#"><i class="fa fa-behance"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- End footer Area -->
