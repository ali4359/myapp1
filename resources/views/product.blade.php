<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
<Style>


.image {
	margin-top:20px;
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
  
}




.maincard:hover .middle {
  opacity: 1;
}

.maincard:hover .image {
  opacity: 0.3;
}


.maincard:hover .info{
  opacity: 0.6;
}

.text {
  background-color:black;
  color: white;
  font-size: 16px;
  padding: 16px 32px;
}
</Style>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="CodePixar">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>Karma Shop</title>
	<!--
		CSS
        ============================================= -->
	
	<link rel="stylesheet" href="css/linearicons.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/themify-icons.css">
	<link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/nice-select.css">
	<link rel="stylesheet" href="css/nouislider.min.css">
	<link rel="stylesheet" href="css/ion.rangeSlider.css" />
	<link rel="stylesheet" href="css/ion.rangeSlider.skinFlat.css" />
    <link rel="stylesheet" href="css/magnific-popup.css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> 

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/ProductCategory.css">
<link rel="stylesheet" href="css/style3.css">
<script src="/js/tog.js"></script>



<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="/js/amazonmenu.js"></script>
<link rel="stylesheet" href="css/amazonmenu.css">
<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu1'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu2'
		
	})
})

</script>



<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu3'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu4'
		
	})
})

</script>






<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body style="background-color:black">
@include('layout.menubar')
<div class="jumbotron jumbotron-fluid" style="background-image:url('/img/j3.jpg');background-repeat: no-repeat, repeat;background-size: cover;margin-top:100px">
  <div class="container">
    <h1 class="display-4" style="color:whitesmoke;text-align:center">MMA</h1>
  </div>
</div>


<Section>
	<div class="container sub-cat-list">
		<h2 style="color:white;font-size: 14px;font-family: 'Fira Sans',sans-serif !important;">MMA requires a blend of all the RIGHT attributes to make you a complete Mixed Martial Artist. Like the sport, our MMA grappling gloves are made of the same things - unbreakable material, flawless performance, and matchless strength. Find all the MMA gear and supplies for the beginner to the pro MMA fighter.</h2>
		
		<div class="row">
		<div class="col-sm-4 maincard" >
      <div class="panel panel-primary" style="background-color:rgba(255, 255, 255, 0.6); border-color:black">
        <div class="panel-body"><img class="image" src="https://media.rdxsports.com/catalog/category/450_x_360_MMA_Gloves__2.jpg" class="img-responsive" style="width:100%" alt="Image"></div>
		<div class="middle">
    <div class="text btn btn-outline-dark"><a href="/shop" style="color:white">Explore</a></div>
  </div>
		<div class="info">
                        <h2 class="sub-cat-name">Boxing Gloves</h2>
                    </div>
      </div>
	</div>
	<div class="col-sm-4 maincard">
       <div class="panel panel-primary" style="background-color:rgba(255, 255, 255, 0.6); border-color:black">
        <div class="panel-body"><img class="image" src="https://media.rdxsports.com/catalog/category/450_x_360_Punch_Bags_2.jpg" class="img-responsive" style="width:100%" alt="Image"></div>
		  <div class="middle">
     <div class="text btn btn-outline-dark"><a href="/shop" style="color:white">Explore</a></div>
        </div>
		  <div class="info">
                        <h2 class="sub-cat-name">Boxing Gloves</h2>
                    </div>
      </div>
	</div>
	<div class="col-sm-4 maincard">
      <div class="panel panel-primary" style="background-color:rgba(255, 255, 255, 0.6); border-color:black">
        <div class="panel-body"><img class="image" src="https://media.rdxsports.com/catalog/category/450_x_360_Training_Gear_.jpg" class="img-responsive" style="width:100%" alt="Image"></div>
		<div class="middle">
    <div class="text btn btn-outline-dark"><a href="/shop" style="color:white">Explore</a></div>
  </div>
		<div class="info">
                        <h2 class="sub-cat-name">Boxing Gloves</h2>
                    </div>
      </div>
	</div>

	<div class="col-sm-4 maincard">
      <div class="panel panel-primary" style="background-color:rgba(255, 255, 255, 0.6); border-color:black">
        <div class="panel-body"><img class="image" src="https://media.rdxsports.com/catalog/category/Protective_Gear.jpg" class="img-responsive" style="width:100%" alt="Image"></div>
		<div class="middle">
    <div class="text btn btn-outline-dark"><a href="/shop" style="color:white">Explore</a></div>
  </div>
		<div class="info">
                        <h2 class="sub-cat-name">Boxing Gloves</h2>
                    </div>
      </div>
	</div>
	<div class="col-sm-4 maincard">
      <div class="panel panel-primary" style="background-color:rgba(255, 255, 255, 0.6); border-color:black">
        <div class="panel-body"><img class="image" src="https://media.rdxsports.com/catalog/category/mma-training-eqipment..jpg" class="img-responsive" style="width:100%" alt="Image"></div>
		<div class="middle">
    <div class="text btn btn-outline-dark"><a href="/shop" style="color:white">Explore</a></div>
  </div>
		<div class="info">
                        <h2 class="sub-cat-name">Boxing Gloves</h2>
                    </div>
      </div>
	</div>
	<div class="col-sm-4 maincard">
      <div class="panel panel-primary" style="background-color:rgba(255, 255, 255, 0.6); border-color:black">
        <div class="panel-body"><img class="image" src="https://media.rdxsports.com/catalog/category/mma-apparel-cetagory_1.jpg" class="img-responsive" style="width:100%" alt="Image"></div>
		<div class="middle">
    <div class="text btn btn-outline-dark"><a href="/shop" style="color:white">Explore</a></div>
  </div>
		<div class="info">
                        <h2 class="sub-cat-name">Boxing Gloves</h2>
                    </div>
      </div>
	</div>
	<div class="col-sm-4 maincard">
      <div class="panel panel-primary" style="background-color:rgba(255, 255, 255, 0.6); border-color:black">
        <div class="panel-body"><img class="image" src="https://media.rdxsports.com/catalog/category/traning-bag-cetagory.jpg" class="img-responsive" style="width:100%" alt="Image"></div>
		<div class="middle">
    <div class="text btn btn-outline-dark"><a href="/shop" style="color:white">Explore</a></div>
  </div>
		<div class="info">
                        <h2 class="sub-cat-name">Boxing Gloves</h2>
                    </div>
      </div>
	</div>
	<div class="col-sm-4 maincard">
      <div class="panel panel-primary" style="background-color:rgba(255, 255, 255, 0.6); border-color:black">
        <div class="panel-body"><img class="image" src="https://media.rdxsports.com/catalog/category/450_x_360_MMA_Gloves__2.jpg" class="img-responsive" style="width:100%" alt="Image"></div>
		<div class="middle">
    <div class="text btn btn-outline-dark"><a href="/shop" style="color:white">Explore</a></div>
  </div>
		<div class="info">
                        <h2 class="sub-cat-name">Boxing Gloves</h2>
                    </div>
      </div>
	</div>
		<div class="col-sm-4 maincard">
      <div class="panel panel-primary" style="background-color:rgba(255, 255, 255, 0.6); border-color:black">
        <div class="panel-body"><img class="image" src="https://media.rdxsports.com/catalog/category/Protective_Gear.jpg" class="img-responsive" style="width:100%" alt="Image"></div>
		<div class="middle">
    <div class="text btn btn-outline-dark"><a href="/shop" style="color:white">Explore</a></div>
  </div>
		<div class="info">
                        <h2 class="sub-cat-name">Boxing Gloves</h2>
                    </div>
      </div>
    </div>
	</div>
</Section>




<Section>
<div class="container">

<h3 class="h3" style="color:white">You May Also Like</h3>
	<div class="row">
		<div class="MultiCarousel" data-items="1,2,2,3" data-slide="1" id="MultiCarousel"  data-interval="1000">
          
            <div class="MultiCarousel-inner">
            <div class="col-md-3 col-sm-6 item">
            <div class="product-grid">
                <div class="product-image">
                    <a href="#">
                        <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-1.jpg">
                        <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-2.jpg">
                    </a>
                    <ul class="social">
                        <li><a href="" data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                        <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                        <li><a href="" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                    </ul>
                    <span class="product-new-label">Sale</span>
                    <span class="product-discount-label">20%</span>
                </div>
                <ul class="rating">
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star disable"></li>
                </ul>
                <div class="product-content">
                    <h3 class="title"><a href="#">Women's Blouse</a></h3>
                    <div class="price">$16.00
                        <span>$20.00</span>
                    </div>
                    <a class="add-to-cart" href="">+ Add To Cart</a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 item">
            <div class="product-grid">
                <div class="product-image">
                    <a href="#">
                        <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-1.jpg">
                        <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-2.jpg">
                    </a>
                    <ul class="social">
                        <li><a href="" data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                        <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                        <li><a href="" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                    </ul>
                    <span class="product-new-label">Sale</span>
                    <span class="product-discount-label">20%</span>
                </div>
                <ul class="rating">
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star disable"></li>
                </ul>
                <div class="product-content">
                    <h3 class="title"><a href="#">Women's Blouse</a></h3>
                    <div class="price">$16.00
                        <span>$20.00</span>
                    </div>
                    <a class="add-to-cart" href="">+ Add To Cart</a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 item">
            <div class="product-grid">
                <div class="product-image">
                    <a href="#">
                        <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-1.jpg">
                        <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-2.jpg">
                    </a>
                    <ul class="social">
                        <li><a href="" data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                        <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                        <li><a href="" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                    </ul>
                    <span class="product-new-label">Sale</span>
                    <span class="product-discount-label">20%</span>
                </div>
                <ul class="rating">
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star disable"></li>
                </ul>
                <div class="product-content">
                    <h3 class="title"><a href="#">Women's Blouse</a></h3>
                    <div class="price">$16.00
                        <span>$20.00</span>
                    </div>
                    <a class="add-to-cart" href="">+ Add To Cart</a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 item">
            <div class="product-grid">
                <div class="product-image">
                    <a href="#">
                        <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-1.jpg">
                        <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-2.jpg">
                    </a>
                    <ul class="social">
                        <li><a href="" data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                        <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                        <li><a href="" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                    </ul>
                    <span class="product-new-label">Sale</span>
                    <span class="product-discount-label">20%</span>
                </div>
                <ul class="rating">
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star disable"></li>
                </ul>
                <div class="product-content">
                    <h3 class="title"><a href="#">Women's Blouse</a></h3>
                    <div class="price">$16.00
                        <span>$20.00</span>
                    </div>
                    <a class="add-to-cart" href="">+ Add To Cart</a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 item">
            <div class="product-grid">
                <div class="product-image">
                    <a href="#">
                        <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-1.jpg">
                        <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-2.jpg">
                    </a>
                    <ul class="social">
                        <li><a href="" data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                        <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                        <li><a href="" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                    </ul>
                    <span class="product-new-label">Sale</span>
                    <span class="product-discount-label">20%</span>
                </div>
                <ul class="rating">
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star disable"></li>
                </ul>
                <div class="product-content">
                    <h3 class="title"><a href="#">Women's Blouse</a></h3>
                    <div class="price">$16.00
                        <span>$20.00</span>
                    </div>
                    <a class="add-to-cart" href="">+ Add To Cart</a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 item">
            <div class="product-grid">
                <div class="product-image">
                    <a href="#">
                        <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-1.jpg">
                        <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-2.jpg">
                    </a>
                    <ul class="social">
                        <li><a href="" data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                        <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                        <li><a href="" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                    </ul>
                    <span class="product-new-label">Sale</span>
                    <span class="product-discount-label">20%</span>
                </div>
                <ul class="rating">
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star disable"></li>
                </ul>
                <div class="product-content">
                    <h3 class="title"><a href="#">Women's Blouse</a></h3>
                    <div class="price">$16.00
                        <span>$20.00</span>
                    </div>
                    <a class="add-to-cart" href="">+ Add To Cart</a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 item">
            <div class="product-grid">
                <div class="product-image">
                    <a href="#">
                        <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-1.jpg">
                        <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-2.jpg">
                    </a>
                    <ul class="social">
                        <li><a href="" data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                        <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                        <li><a href="" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                    </ul>
                    <span class="product-new-label">Sale</span>
                    <span class="product-discount-label">20%</span>
                </div>
                <ul class="rating">
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star disable"></li>
                </ul>
                <div class="product-content">
                    <h3 class="title"><a href="#">Women's Blouse</a></h3>
                    <div class="price">$16.00
                        <span>$20.00</span>
                    </div>
                    <a class="add-to-cart" href="">+ Add To Cart</a>
                </div>
            </div>
        </div>
            </div>
            <button class="btn btn-primary leftLst"><</button>
            <button class="btn btn-primary rightLst">></button>
        </div>
	</div>

</div>
</Section>


@include('layout.Footer')







<script type="text/javascript">
$(document).ready(function () {
    var itemsMainDiv = ('.MultiCarousel');
    var itemsDiv = ('.MultiCarousel-inner');
    var itemWidth = "";

    $('.leftLst, .rightLst').click(function () {
        var condition = $(this).hasClass("leftLst");
        if (condition)
            click(0, this);
        else
            click(1, this)
    });

    ResCarouselSize();




    $(window).resize(function () {
        ResCarouselSize();
    });

    //this function define the size of the items
    function ResCarouselSize() {
        var incno = 0;
        var dataItems = ("data-items");
        var itemClass = ('.item');
        var id = 0;
        var btnParentSb = '';
        var itemsSplit = '';
        var sampwidth = $(itemsMainDiv).width();
        var bodyWidth = $('body').width();
        $(itemsDiv).each(function () {
            id = id + 1;
            var itemNumbers = $(this).find(itemClass).length;
            btnParentSb = $(this).parent().attr(dataItems);
            itemsSplit = btnParentSb.split(',');
            $(this).parent().attr("id", "MultiCarousel" + id);


            if (bodyWidth >= 1200) {
                incno = itemsSplit[3];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 992) {
                incno = itemsSplit[2];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 768) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
            }
            else {
                incno = itemsSplit[0];
                itemWidth = sampwidth / incno;
            }
            $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
            $(this).find(itemClass).each(function () {
                $(this).outerWidth(itemWidth);
            });

            $(".leftLst").addClass("over");
            $(".rightLst").removeClass("over");

        });
    }


    //this function used to move the items
    function ResCarousel(e, el, s) {
        var leftBtn = ('.leftLst');
        var rightBtn = ('.rightLst');
        var translateXval = '';
        var divStyle = $(el + ' ' + itemsDiv).css('transform');
        var values = divStyle.match(/-?[\d\.]+/g);
        var xds = Math.abs(values[4]);
        if (e == 0) {
            translateXval = parseInt(xds) - parseInt(itemWidth * s);
            $(el + ' ' + rightBtn).removeClass("over");

            if (translateXval <= itemWidth / 2) {
                translateXval = 0;
                $(el + ' ' + leftBtn).addClass("over");
            }
        }
        else if (e == 1) {
            var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
            translateXval = parseInt(xds) + parseInt(itemWidth * s);
            $(el + ' ' + leftBtn).removeClass("over");

            if (translateXval >= itemsCondition - itemWidth / 2) {
                translateXval = itemsCondition;
                $(el + ' ' + rightBtn).addClass("over");
            }
        }
        $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }

    //It is used to get some elements from btn
    function click(ell, ee) {
        var Parent = "#" + $(ee).parent().attr("id");
        var slide = $(Parent).attr("data-slide");
        ResCarousel(ell, Parent, slide);
    }

});
</script>
    
    

	<script src="js/vendor/jquery-2.2.4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	 crossorigin="anonymous"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/jquery.nice-select.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/nouislider.min.js"></script>
	<script src="js/countdown.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
	<script src="instafeed.min.js"></script>
	<script src="custom.js"></script>
	
    
    
  
   
	<!--gmaps Js-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="js/gmaps.min.js"></script>
    <script src="js/main.js"></script>
    
   
</body>

</html>