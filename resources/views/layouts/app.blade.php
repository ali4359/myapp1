<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>


<script src="js/vendor/jquery-2.2.4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	 crossorigin="anonymous"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/jquery.nice-select.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/nouislider.min.js"></script>
	<script src="js/countdown.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
	<script src="instafeed.min.js"></script>
	<script src="custom.js"></script>


<link rel="stylesheet" href="css/bootstrap.css">
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link rel="stylesheet" href="css/linearicons.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/themify-icons.css">
	<link rel="stylesheet" href="css/amazonmenu.css">

	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/nice-select.css">
	<link rel="stylesheet" href="css/nouislider.min.css">
	<link rel="stylesheet" href="css/ion.rangeSlider.css" />
	<link rel="stylesheet" href="css/ion.rangeSlider.skinFlat.css" />
	<link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/main.css">
	
	<link rel="stylesheet" href="css/style.css">
    <link href="assets/owl.carousel.min.css" rel="stylesheet"/>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/style3.css">
<script src="/js/tog.js"></script>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="/js/amazonmenu.js"></script>
<link rel="stylesheet" href="css/amazonmenu.css">



<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu1'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu2'
		
	})
})

</script>



<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu3'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu4'
		
	})
})

</script>
















    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

   
    <!-- Scripts -->
   <!-- <script src="{{ asset('js/app.js') }}" defer></script>  -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <!--  <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
</head>
<body>
@include('layout.menubar')


<div class="container-fluid">
  <div class="row">
    <div class="col-md-3">

    @if(Route::is('register'))


    <img src="/img/reg.jpg" width="600px">

    @else

    
      <img src="/img/login1.jpg" width="600px">

    @endif

   
    </div>
    


    <div class="col-md-3">
 @if(Route::is('login'))
<img src="/img/lo1.png"    width="600px">

@endif

 <div id="app">
      

        <main class="py-4"  style="margin-top:35%; margin-left:8%">
            @yield('content')
        </main>
    </div>


    </div>
  </div>

   
</body>
</html>
