<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="CodePixar">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>Karma Shop</title>
	<!--
		CSS
		============================================= -->
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> 
	
	<link rel="stylesheet" href="css/linearicons.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/themify-icons.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/nice-select.css">
	<link rel="stylesheet" href="css/nouislider.min.css">
	<link rel="stylesheet" href="css/ion.rangeSlider.css" />
	<link rel="stylesheet" href="css/ion.rangeSlider.skinFlat.css" />
	<link rel="stylesheet" href="css/magnific-popup.css">
	
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/custom.css">
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/style3.css">
<script src="/js/tog.js"></script>
	
	

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="/js/amazonmenu.js"></script>
<link rel="stylesheet" href="css/amazonmenu.css">
<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu1'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu2'
		
	})
})

</script>



<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu3'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu4'
		
	})
})

</script>






<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>

	<!-- Start Header Area -->
	@include('layout.menubar')
	<!-- End Header Area -->
    
	<!-- start banner Area -->
	<section class="banner-area" style="background: url(/img/banner/harrier-banner.jpg) center no-repeat;">
	<div style="height:600px">
	
	</div>
	</section>
	<!-- End banner Area -->

    <!-- start features Area -->
    <section class="features-area" style="background-color:#3f2d22">
		<div class="row justify-content-center">
				<div class="col-lg-6 text-center">
					<div class="section-title">
					<h3 style="color:#ffffff">You’ve waited long enough.<br>
					Observing your adversaries every move, you’ve evolved into the perfect <br>opponent. You’re ready to invade and take it all,<br>
					RDX <b>HARRIER</b> with <b>NOVA-TECH</b> (Patent Pending)</h3>
					</div>
					</div>
					</div>
	

	<!-- Start Boxing Area -->
<div class="container-fluid part-one" style="background-color:#3f2d22">
	<div class="row">
					<div class="col-md-3 d-none d-sm-none d-md-none d-lg-block d-xl-block">
					    <div class="sideimage" style="position: static; top: 0px; width: auto;">
					        <img  src="img/elements/leftside-image.png" alt="grappling glove" data-alt-src="img/category/grapling-glove-hover.jpg">
					    </div>
					</div>	
					<div class="col-md-12 col-sm-12 col-lg-6 col-xl-6 justify-content-center ">
					<div>
					<img class="img-fluid pr rp" src="img/elements/gloves-title.png" alt="" data-alt-src="img/category/mma-glove-hover.jpg">
					<img class="img-fluid pr rp" src="img/elements/boxingglove.png" alt="" data-alt-src="img/category/mma-glove-hover.jpg">
					</div>
					<div style="background-color:#3f2d22">
					<h3 style="color:#ffffff"><b>MG-3</b></h3>  <p> Mould that secures the wrists in perfect form and 
					makes it possible for the glove to house compressed EVA, Blacktop foam, and Nova-Tech,
					 giving you unmatched security while maintaining agility.</p>
                    
					 <h3 style="color:#ffffff"><b>Nova-Tech (Patent Pending) - </b></h3>
					<p>  RDX’s proprietary Flat Arc Radial Panel that decreases impact intensity by dispersing force on a smooth,
					   radial, surface.</p>
					</div>
					<div class="product-btn ">
					<button class="button bttn" target="_blank">SHOP NOW</a>
					</div>
					</div>
					<div class="col-md-3 d-none d-sm-none d-md-none d-lg-block d-xl-block">
					    <div class="sideimage-right" style="position: static; top: 0px; right: 0px; width: auto;">	
					        <img class="img-fluid pr rp" src="img/elements/rightside-image.png" alt="" data-alt-src="img/category/boxing-glove-hover.jpg">
                        </div>
					</div>	
	</div>
</div>
			
<!-- FocusPads Area -->
<div class="container-fluid part-two text-center" style="background-color:#3f2d22" >
	<div class="row justify-content-center">
	    <div class="col-md-12 col-sm-12 col-lg-6 col-xl-6">
			<div>
			<img class="img-fluid " src="img/elements/focuspad-title.png" alt="">
			<img class="img-fluid " src="img/elements/focuspad.png" alt="">				
		   </div>
		   <div style="background-color:#3f2d22">
		   <h3 style="color:#ffffff"><b>MG-3 - </b></h3><p> Mould that secures the wrists in perfect form and 
					makes it possible for the glove to house compressed EVA, Blacktop foam, and Nova-Tech,
					 giving you unmatched security while maintaining agility.</p>
                     <h3 style="color:#ffffff"><b>
					 Nova-Tech (Patent Pending) -</b></h3> 
					 <p>
					  RDX’s proprietary Flat Arc Radial Panel that decreases impact intensity by dispersing force on a smooth,
					   radial, surface.</p>
					</div>
					<div class="product-btn ">
					<button class="button bttn " target="_blank">SHOP NOW</a>
					</div>
			</div>
		</div>
		</div>
	</div>
</div>			
	 

	<!---Tshirt Area-->	
	<div class="container-fluid part-two text-center" style="background-color:#3f2d22;padding-top:10px" >
	<div class="row justify-content-center">
	    <div class="col-md-12 col-sm-12 col-lg-6 col-xl-6">
			<div>
			<img class="img-fluid pr rp" src="img/elements/tshirt-title.png" alt="">
			<img class="img-fluid pr rp" src="img/elements/tshirt-main.png" alt="">				
		   </div>
		   <div style="background-color:#3f2d22">
					<p>With focus on breathable construction,
						 experience a more pleasant and comfortable fit for better training sessions.<br>
							Performance polyester battles shrinkage and
							 wrinkling at micro level by binding molecules tightly together.
</p>
					</div>
					<div class="product-btn ">
					<button class="button bttn " target="_blank">SHOP NOW</a>
					</div>
			</div>
		</div>
	</div>
</div>

<!---Bunch Area-->

<div class="container-fluid part-two text-center" style="background-color:#3f2d22" >
	<div class="row justify-content-center">
	    <div class="col-md-12 col-sm-12 col-lg-6 col-xl-6">
			<div>
			<img class="img-fluid pr rp" src="img/elements/bundle-title.png" style="padding-top:10px" alt="">
			<img class="img-fluid pr rp" src="img/elements/bundle-main.png" alt="">				
		   </div>
		   <div style="background-color:#3f2d22">
					<p>Get the complete package with the</p>
					<h3 style="color:#ffffff"><b>RDX Harrier bundle</b></h3><p> in a stylish, durable drawstring bag.
</p>
					</div>
					<div class="product-btn ">
					<button class="button bttn " target="_blank">SHOP NOW</a>
					</div>
			</div>
		</div>
	</div>
</div>
	<!-- start footer Area -->
@include('layout.Footer')
	<!-- End footer Area -->

	<script src="js/vendor/jquery-2.2.4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	 crossorigin="anonymous"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/jquery.nice-select.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/nouislider.min.js"></script>
	<script src="js/countdown.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<!--gmaps Js-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="js/gmaps.min.js"></script>
	<script src="js/main.js"></script>
    <script>
    $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
	autoplay:true,
	autoplayTimeout:2000,
	navigation:true,
	navigationText: ["◀ Left <strong>arrow</strong>","Right <strong>arrow</strong> ▶"],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
	</script>



</body>

</html>