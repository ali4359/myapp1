
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1,shrink-to-fit=no">
    <!-- Favicon-->
	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="CodePixar">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
    <title>Product</title>
    
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="css/linearicons.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/themify-icons.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/nice-select.css">
  <link rel="stylesheet" href="css/nouislider.min.css">
  
  <link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/ion.rangeSlider.css" />
	<link rel="stylesheet" href="css/ion.rangeSlider.skinFlat.css" />
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/custom2.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> 

    
    <link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/ProductCategory.css">
    <link rel="stylesheet" href="css/style3.css">
    <script src="/js/tog.js"></script>

    

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="/js/amazonmenu.js"></script>
<link rel="stylesheet" href="css/amazonmenu.css">
<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu1'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu2'
		
	})
})

</script>



<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu3'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu4'
		
	})
})

</script>






<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
  </head>

  <body>
  @include('layout.menubar')
<div class="jumbotron jumbotron-fluid" style="background-image:url('/img/j3.jpg');background-repeat: no-repeat, repeat;background-size: cover;margin-top:100px">
  <div class="container">
    <h1 class="display-4" style="color:whitesmoke;text-align:center">Product Details</h1>
  </div>
  </div>

  <!-- Product Area -->
  <div class="container">
  <div class="row" style="background-color:#222">
  <div class="col-lg-12">
  <div class="row">
  
  <div class="col-lg-6 col-md-3 " style="background-color:white">
  <div class="rm">
  <div class="mySlides">
    <div class="numbertext" style="width:100%">1 / 6</div>
    <img  src="img/product/k1.jpg" >
  </div>

  <div class="mySlides">
    <div class="numbertext" style="width:100%">2 / 6</div>
    <img  src="img/product/k2.jpg" >
  </div>

  <div class="mySlides">
    <div class="numbertext" style="width:100%">3 / 6</div>
    <img  src="img/product/k3.jpg" >
  </div>
    
  <div class="mySlides">
    <div class="numbertext" style="width:100%">4 / 6</div>
    <img  src="img/product/k4.jpg" >
  </div>

  <div class="mySlides">
    <div class="numbertext" style="width:100%">5 / 6</div>
    <img  src="img/product/k5.jpg" >
  </div>
    
  <div class="mySlides">
    <div class="numbertext" style="width:100%">6 / 6</div>
    <img  src="img/product/k6.jpg" >
  </div>
    
  <a class="prev" onclick="plusSlides(-1)">❮</a>
  <a class="next" onclick="plusSlides(1)">❯</a>

</div>
   </div>
   <div class="col-lg-6 col-md-6">
   <div class="caption-container">
    <p id="caption"></p>
  </div>

  <div class="caption-container2">
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<p >Reviews|<a>Write a Review</a></p>
  </div>

 <div class="caption-container2">
  <p id="height"></p>
   </div>
<p >Choose Your Product Height</p>
<select onchange="myFunction(this)">
  <option value="4ft">4ft</option>
  <option value="5ft">5ft</option>
</select>
<div class="caption-container2">
  <p> Price: $XXXX</p>
  <p>Weight: 18-20 Kg</p>
   </div>
<p  style="font-size:16px">Standard Shipping : </p>
<p >Arrives in 4-6 Business Days</p>
<p  style="font-size:16px">Qty: </p>
<input type="number">
<br></br>
<button class="btn-1">Add To Cart</button>
<button class="btn-2">Buy Now</button>
<br></br>
<img  style="height:50px;width:50px" src="img/organic-food/visa.svg">
<img  style="height:50px;width:50px" src="img/organic-food/discover.svg">
<img  style="height:50px;width:50px" src="img/organic-food/mastercard.svg">
<img style="height:50px;width:50px" src="img/organic-food/maestro.svg">
<img  style="height:50px;width:50px" src="img/organic-food/diners.svg">
<img  style="height:50px;width:50px" src="img/organic-food/jcb.svg">
<img style="height:50px;width:50px" src="img/organic-food/paypal.svg">
</div>


  <div class="row">
    <div class="col-lg-12">
    <div class="column">
      <img class="demo cursor" src="img/product/k1.jpg" style="width:100%" onclick="currentSlide(1)" alt="The Woods">
    </div>
    <div class="column">
      <img class="demo cursor" src="img/product/k2.jpg" style="width:100%" onclick="currentSlide(2)" alt="Cinque Terre">
    </div>
    <div class="column">
      <img class="demo cursor" src="img/product/k3.jpg" style="width:100%" onclick="currentSlide(3)" alt="Mountains and fjords">
    </div>
    <div class="column">
      <img class="demo cursor" src="img/product/k4.jpg" style="width:100%" onclick="currentSlide(4)" alt="Northern Lights">
    </div>
    <div class="column">
      <img class="demo cursor" src="img/product/k5.jpg" style="width:100%" onclick="currentSlide(5)" alt="Nature and sunrise">
    </div>    
    <div class="column">
      <img class="demo cursor" src="img/product/k6.jpg" style="width:100%" onclick="currentSlide(6)" alt="Snowy Mountains">
    </div>
</div>
  </div>
  </div>
  </div>

</div>    
</div>
<!--Product Information Area-->
<div class="container" style="background-color:black">
<div class="row">
  <div class="col-lg-12">
  <div class="row" style="background-color:#222" >
  <div class="col-lg-9">  
    <p style="font-size:30px;padding-top:14px;padding-left:8px">More About RDX T17 Aura Punch Bag</p>
    <p style="font-size:14px;padding-top:5px;padding-left:8px">Made for pros and amateurs alike. Quality that
       will match your intensity strike by strike.
        Bring out your best boxing, Muay Thai or striking skills
         on a heavy bag designed to make you break a sweat.
          The Aura punching bag is twin-layered for maximum durability and
           longer than average product life.
            Bring out your inner fire with confidence knowing your punch bag
             is here to stay.
              Duly stitched, textile filled heavy duty punch bag weighs (approx. 18- 20 kg, 4ft)
               and (approx.20-22kg 5ft). Unfilled 4ft punch bag can be filled up to (20kg)
                and
       Unfilled 5ft punch bag can be filled up to (22kg)</p>
</div>
</div>
<div class="row">
   <div class="col-lg-9">
    <p style="font-size:24px;padding-top:10px;padding-left:8px">Shipment Companies :</p>
    <img  class="img-fluid" style="padding-bottom:15px" src="img/brand/Dealers.png">
  </div>

</div></div>
</div>
</div>


	<!-- start footer Area -->
	<footer class="footer-area section_gap">
		<div class="container">
			<div class="row">
				<div class="col-lg-3  col-md-6 col-sm-6">
					<div class="single-footer-widget">
						
				
						<h6>About Us</h6>
						<a href="#" style="color:white">Boxing</a><br>
						<a href="#" style="color:white">MMA</a><br>
						<a href="#" style="color:white">Fitness</a><br>
						<a href="#" style="color:white">Clothing</a><br>
						<a href="#" style="color:white">Sports</a><br>
						<a href="#" style="color:white">Events</a>
						
					</div>
				</div>
				<div class="col-lg-4  col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h6>Media</h6>
						<a href="#" style="color:white">Coperate News</a><br>
						<a href="#" style="color:white">Fitness Tips</a><br>
						<a href="#" style="color:white">RDX TV</a><br>
						<a href="#" style="color:white">Brand Ambassadors</a><br>
						<a href="#" style="color:white">WholeSale Blog</a><br>
						<a href="#" style="color:white">Social Shop</a><br>
						<a href="#" style="color:white">CEO Message</a><br>
					</div>
				</div>
				<div class="col-lg-3  col-md-6 col-sm-6">
					<div class="single-footer-widget mail-chimp">
						<h6 class="mb-20">Company</h6>
						<a href="#" style="color:white">About us</a><br>
						<a href="#" style="color:white">Affliate Program</a><br>
						<a href="#" style="color:white">Wholesale</a><br>
						<a href="#" style="color:white">Trade Catalog</a><br>
						<a href="#" style="color:white">WholeSale</a><br>
						<a href="#" style="color:white">Technology</a><br>
						<a href="#" style="color:white">Contact us</a><br>
					</div>
				</div>
				<div class="col-lg-2 col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h6>Customer Help</h6>
						
						
						<a href="#" style="color:white">FAQ</a><br>
						<a href="#" style="color:white">Site Chart</a><br>
						<a href="#" style="color:white">RDX Authenticator</a><br>
						<a href="#" style="color:white">Shipment & Conditions</a><br>
						<a href="#" style="color:white">Terms & Conditions</a><br>
						<a href="#" style="color:white">Privacy Policy</a><br>
						<a href="#" style="color:white">Return Policy</a><br>
						
					</div>
				</div>
			</div>
			<div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
				
			</div>

			
				<hr style="background-color:red">

				<div class="row">
				<div class="col-lg-3  col-md-6 col-sm-6">
					<div class="single-footer-widget">
						
					
						<h4 style="color:white">Call Us +44 808 189 4444</h4>
						<p>© 1999-2020 RDX LLC. All rights reserved.</p>
					</div>
				</div>
				<div class="col-lg-4  col-md-6 col-sm-6">
					
				</div>
				<div class="col-lg-3  col-md-6 col-sm-6">
				
				</div>
				<div class="col-lg-2 col-md-6 col-sm-6">
					<div class="single-footer-widget">
					<h4 style="color:white">Let us be social</h4>
						<div class="footer-social d-flex align-items-center">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-dribbble"></i></a>
							<a href="#"><i class="fa fa-behance"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- End footer Area -->



<!--Start of script-->
    <script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

<!-- Script for slide-->
<!-- Script for height-->
<script>
function myFunction(selTag) {
  var x = selTag.options[selTag.selectedIndex].text;
  document.getElementById("height").innerHTML = "Height: " + x;
}
</script>
<!--- -->

<!-- Script for width-->
<script>
function myFunction1(selTag) {
  var y = selTag.options[selTag.selectedIndex].text;
  document.getElementById("width").innerHTML = "Width: " + y;
}
</script>
<!---Tab Content -->
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>

<!--Tab Script ended -->
    <script type="text/javascript">
$(document).ready(function () {
    var itemsMainDiv = ('.MultiCarousel');
    var itemsDiv = ('.MultiCarousel-inner');
    var itemWidth = "";

    $('.leftLst, .rightLst').click(function () {
        var condition = $(this).hasClass("leftLst");
        if (condition)
            click(0, this);
        else
            click(1, this)
    });

    ResCarouselSize();




    $(window).resize(function () {
        ResCarouselSize();
    });

    //this function define the size of the items
    function ResCarouselSize() {
        var incno = 0;
        var dataItems = ("data-items");
        var itemClass = ('.item');
        var id = 0;
        var btnParentSb = '';
        var itemsSplit = '';
        var sampwidth = $(itemsMainDiv).width();
        var bodyWidth = $('body').width();
        $(itemsDiv).each(function () {
            id = id + 1;
            var itemNumbers = $(this).find(itemClass).length;
            btnParentSb = $(this).parent().attr(dataItems);
            itemsSplit = btnParentSb.split(',');
            $(this).parent().attr("id", "MultiCarousel" + id);


            if (bodyWidth >= 1200) {
                incno = itemsSplit[3];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 992) {
                incno = itemsSplit[2];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 768) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
            }
            else {
                incno = itemsSplit[0];
                itemWidth = sampwidth / incno;
            }
            $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
            $(this).find(itemClass).each(function () {
                $(this).outerWidth(itemWidth);
            });

            $(".leftLst").addClass("over");
            $(".rightLst").removeClass("over");

        });
    }


    //this function used to move the items
    function ResCarousel(e, el, s) {
        var leftBtn = ('.leftLst');
        var rightBtn = ('.rightLst');
        var translateXval = '';
        var divStyle = $(el + ' ' + itemsDiv).css('transform');
        var values = divStyle.match(/-?[\d\.]+/g);
        var xds = Math.abs(values[4]);
        if (e == 0) {
            translateXval = parseInt(xds) - parseInt(itemWidth * s);
            $(el + ' ' + rightBtn).removeClass("over");

            if (translateXval <= itemWidth / 2) {
                translateXval = 0;
                $(el + ' ' + leftBtn).addClass("over");
            }
        }
        else if (e == 1) {
            var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
            translateXval = parseInt(xds) + parseInt(itemWidth * s);
            $(el + ' ' + leftBtn).removeClass("over");

            if (translateXval >= itemsCondition - itemWidth / 2) {
                translateXval = itemsCondition;
                $(el + ' ' + rightBtn).addClass("over");
            }
        }
        $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }

    //It is used to get some elements from btn
    function click(ell, ee) {
        var Parent = "#" + $(ee).parent().attr("id");
        var slide = $(Parent).attr("data-slide");
        ResCarousel(ell, Parent, slide);
    }

});
</script>
    
<script src='//production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js'></script><script src='//raw.githubusercontent.com/greenwoodents/quickbeam.js/master/dist/quickbeam.min.js'></script><script src='//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/TweenMax.min.js'></script>
<script >//Hey you! Thanks for checking this pen. :) I open source it as library. https://github.com/greenwoodents/quickbeam.js

var Quickbeam = (function () {
  // Instance stores a reference to the Singleton
  var instance;

  function init(att) {
    // Singleton
    var els = {};
    var self = {};

    //Select attributes
    var cart = document.querySelector('#quick-cart');
    var cartPay = document.querySelector('#quick-cart-pay');
    var cartPrice = document.querySelector('#quick-cart-price');

    var addToCart = document.querySelector('[quickbeam="add-to-cart"]');
    var ProductImage = document.querySelector('[quickbeam="image"]');
    var price = document.querySelector('[quickbeam="price"]');

    var variantId;
    var imageUrl;
    var count;
    var color = '#000';

    var last_removed_variant;
    var last_removed_variant_count;


    (function main() {

      window.onresize = function(event) {
        setPayButtonAction();
      };

      setPayButtonAction();

      if (cartPay.length > 0) {return false;}

      if (att.animationLib === 'gsap') {
        if (typeof TweenMax !== 'function') {throw "GSAP is not loaded."}
      }

      if (price) {
        price = price.innerHTML;
      }

      if (ProductImage) {
        imageUrl = ProductImage.getAttribute('src');

        if (!imageUrl) {
          var patt = /url\(\s*(['"]?)(.*?)\1\s*\)/i
          imageUrl = ProductImage.getAttribute('style').match(patt)[2];
        }
      }

      [].forEach.call(document.querySelectorAll('.quickbeam-variant'), function(el){
        if (el.checked) {
          variantId = parseInt(el.getAttribute('quickbeam-value'));
        }
      });


      //Add event listeners
      var listeners = {
        '.quick-cart-product-remove': function(el){
            var id = el.getAttribute("data-id");
            var product = el.parentNode;
            var productCount = product.querySelector('.count');
            var count = parseInt(productCount.innerText);
            var imgWrap = product.querySelector('div');

            if (!(product && cart)) {return false}

            count--;

            if (count <= 0) {
              //Animation
              product.classList.add("remove-product");
              window.setTimeout(function() {
                product.classList.remove("remove-product");
                removeProduct(product);
              }, 1000);
            } else {
              productCount.innerText = count;

              var clone = imgWrap.cloneNode(true);
              clone.classList.add('animateOut')
              imgWrap.parentNode.appendChild(clone);

              window.setTimeout(function() {
                clone.parentNode.removeChild(clone);
              }, 1000);


              if (count <= 1) {
                productCount.classList.add("hide");
              }
            }

            if (last_removed_variant == id) {
              last_removed_variant_count++;
            } else {
              last_removed_variant = id;
              last_removed_variant_count = 1;
            }

            if (3 == last_removed_variant_count && count > 1) {
              product.classList.add("show-remove-all");
            }


            ajaxRemoveProduct({quantity: count, id: id});
          },

          '.quick-cart-product-removeall': function(el) {
              var id = el.getAttribute("data-id");
              var product = el.parentNode;

              if (!(product && cart)) {return false}

              product.classList.add("remove-product");
              window.setTimeout(function() {
                removeProduct(product_box);
              }, 200);

              ajaxRemoveProduct({quantity: 0, id: id});
          }
      }

      //Event delegation for cart
      cart.addEventListener("click",function(e) {
        //calling callback if item of object have a match.
        for (var key in listeners) {
          if (listeners.hasOwnProperty(key) && e.target && e.target.matches(key)) {
            listeners[key].apply(null, [e.target]);
          }
        }
      });

      if (addToCart) {
        //Buy button listener.
        addToCart.addEventListener('click', start, false);
      }

      function start(e){
        e.preventDefault();
        this.blur();

        // display pay button if is not
        if (!(cartPay.classList.contains('open'))) {
          cartPay.classList.add("open");
        }

        count = parseInt(document.querySelector('.quantity-selector').value);

        addProduct();

        animateProduct();

        if (ProductImage) {
          ProductImage.classList.add("animate");
          window.setTimeout(function(){
            ProductImage.classList.remove("animate");
          }, 400);
        }
      }

      return false;
    })();

    //Procedure for creating product in cart and displaying after animation.
    function addProduct() {
      var variant;

      //Calling select variant from attributes or falling back to default select variant.
      if (typeof att.variantSelector === 'function') {
        variant = att.variantSelector.call();
        if (typeof variant !== string) {
          console.error("variantSelector not returning a string.");
          variant = '';
        }
      } else {
        [].forEach.call(document.querySelectorAll('.quickbeam-variant'), function(el){
          if (el.checked) {
            variantId = parseInt(el.getAttribute('quickbeam-value'));
            variant = el.getAttribute('value');
          }
        });
      }

      if (typeof variant === 'undefined') {
        console.error("Not able to select variant");
        variant = '';
      }

      var cart_product = document.querySelector("#quick-cart-product-" + variantId) || false;

      if (cart && ProductImage && cart_product == false ) {
        //Create product box
        var element = createProductBox({
          id: variantId,
          price: price,
          image: imageUrl,
          size: variant,
          color: color
        });
        //Append element to cart
        cart.insertBefore(element, cart.firstChild);
        //Display created element
        displayProductBox(element, 1000);
      }
    }

    // Function for creating DOM element from pre-set template.
    // Private function
    // Arguments: variantId, price, swatchesContent
    // Returning created DOM element.
    function createProductBox(data) {
      var template = '<div class="quick-cart-product-wrap">'+
                        '<img src="' + data.image + '">'+
                        '<span class=" s1" style="background-color: '+data.color+'; opacity: .5">'+ data.price.trim() +'</span>'+
                        '<span class=" s2">'+ data.size.trim() +'</span>'+
                      '</div>'+
                      '<span class="count hide fadeUp" id="quick-cart-product-count-'+ data.id +'">0</span>'+
                      '<span class="quick-cart-product-remove remove" data-id="'+ data.id +'"></span>'+
                      '<span class="quick-cart-product-removeall removeall" data-id="'+ data.id +'"></span>';

      var div = document.createElement("div");
      div.classList.add("quick-cart-product");
      div.classList.add("quick-cart-product-static");
      div.setAttribute("id", "quick-cart-product-" + data.id);
      div.style.opacity = 0;
      div.innerHTML = template;

      return div;
    }

    // Function for displaying product box.
    // private function
    // Arguments: createde element, delay of display.
    function displayProductBox(el, delay) {
      //Defaults
      delay = typeof delay !== 'undefined' ? delay : 0;

      window.setTimeout(function(){
        el.style.opacity = 1;
      }, delay);
    }

    //requst animation frame animation function
    function animate(item) {
      var duration = item.time,
      end = +new Date() + duration;

      var step = function() {

        var current = +new Date(),
            remaining = end - current;

        if(remaining < 60) {
          item.run(1);  //1 = progress is at 100%
          return;

        } else {
          var rate = 1 - remaining/duration;
          item.run(rate);
        }

        requestAnimationFrame(step);
      }
      step();
    }

    //Procedure for animating process of adding product box to cart using bezire curve.
    //Private
    function animateProduct() {
      // create and append copy of large image.
      var element = createAnimatedObject();
      var c = getAnimationCoordinations(element);

      if (att.animationLib === 'gsap') {
        gsapAnimation(element, c);
      } else {
        fallbackAnimation(element, c);
      }
    }

    function gsapAnimation(element, c) {
      element.style.position = 'absolute';
      element.classList.add("run");

      var countBox = document.getElementById("quick-cart-product-count-" + variantId);
      if (countBox) {
        countBox.classList.remove('fadeUp')
        countBox.classList.add('fadeDown')
      }

      TweenMax.to(element, 1, {bezier:{type:"soft", values:c.through}, ease:Power1.easeInOut});

      setTimeout(function(){
        element.style.opacity = 0;
        document.body.removeChild(element);

        setTimeout(function(){
          ajaxAddProductToCart();
        },100)
      }, 1000);
    }

    //Vanilla JS Animation
    function fallbackAnimation(element, c) {
      var cordeIndex = 0;
      var coord = function (x,y) {
        if(!x) var x=0; if(!y) var y=0;
        return {x: x, y: y};
      };

      var bezier = function(t, p0, p1, p2, p3){
        var cX = 3 * (p1.x - p0.x),
            bX = 3 * (p2.x - p1.x) - cX,
            aX = p3.x - p0.x - cX - bX;

        var cY = 3 * (p1.y - p0.y),
            bY = 3 * (p2.y - p1.y) - cY,
            aY = p3.y - p0.y - cY - bY;

        var x = (aX * Math.pow(t, 3)) + (bX * Math.pow(t, 2)) + (cX * t) + p0.x;
        var y = (aY * Math.pow(t, 3)) + (bY * Math.pow(t, 2)) + (cY * t) + p0.y;

        return {x: x, y: y};
      };
      //Coordinations
      //Start
      var P1 = coord(c.start.x,c.start.y);
      //HELPERS
      var P2 = coord(c.start.x-300,c.final.y);
      var P3 = coord(c.start.x+500,c.start.y+500);
      //final destination
      var P4 = coord(c.final.x,c.final.y);

      //Actaully animate.
      var stage = 0;
      element.style.position = 'absolute';
      element.classList.add("run");
      animate({
        time: 1000,

        run: function(t) {
          if(t == 1) {
            setTimeout(function(){
              element.style.opacity = 0;
              document.body.removeChild(element);

              setTimeout(function(){
                ajaxAddProductToCart();
              },1000)
            }, 500);

          }
          //find position on bezier curve
          var curpos = bezier(t,P1,P2,P3,P4)

          var trans = "translate("+Math.round(curpos.x)+"px,"+Math.round(curpos.y)+"px)";

          element.style.webkitTransform = trans;
          element.style.transform = trans;
        }
      });
    }

    // Function for creating DOM element from pre-set template.
    // Private function
    // Arguments: none
    // Returning created DOM element.
    function createAnimatedObject(data) {
      var width = ProductImage.offsetWidth - 2;
      var height = Math.round(parseInt(ProductImage.offsetWidth - 2) * 1.33);
      var offset = ProductImage.getBoundingClientRect();

      var template =  '<div style="width:'+ width +'px; height:'+ height +'px;">'+
                      '<img src="'+ imageUrl +'">'+
                      '<span class="s1" style="background-color: '+color+'; opacity: .5; transition: 1000ms"></span>'+
                      '<span class="s2"></span>'+
                      '</div>';

      // Animace pridani produktu k produktovemu boxu
      var div = document.createElement("div");
      div.classList.add("quick-cart-product");
      div.classList.add("quick-cart-product");
      div.classList.add("animated");
      div.setAttribute("id", "quick-cart-product-animated");

      if (att.animationLib === 'gsap') {
        var trans = "translate("+ offset.left +"px,"+ offset.top +"px)";
        div.style.webkitTransform = trans;
        div.style.transform = trans;
      }

      //Apend template
      div.innerHTML = template;
      //Append child to body
      document.body.appendChild(div);
      //return
      return div;
    }

    // Function for calculating animation coordinations
    // Private function
    // Arguments: element from DOM.
    // Returning object { start:{x,y}, finish: {x,y} }.
    function getAnimationCoordinations(element) {
      var child = element.querySelector('div');
      // Calc of start and finish positions of animation.
      var start = ProductImage.getBoundingClientRect();
      var final = document.querySelector("#quick-cart-product-" + variantId).getBoundingClientRect();
      var fTop = final.top;
      // adding scroll heihft to Y
      var doc = document.documentElement;
      var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
      var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);

      var throughX = parseInt(start.left) - parseInt(child.style.width) * 1.4;
      var throughY = (fTop + top) - parseInt(child.style.height) / 3;

      return {
        start: {
          x: start.left,
          y: start.top
        },
        through: [
          {x:throughX, y:throughY},
          {x:final.left, y:fTop + top}
        ],
        final: {
          x: final.left,
          y: fTop + top
        }
      }
    }

    // Function for adding product to shopify cart using AJAX
    // Private function
    // Arguments:
    // Returning nothing, procedure.
    function ajaxAddProductToCart() {
      var product_box = document.getElementById("quick-cart-product-" + variantId);

      var product_count_box = document.getElementById("quick-cart-product-count-" + variantId);
      var product_count = parseInt(product_count_box.innerText);
      var cartPay_total_count = document.getElementById("quick-cart-pay-total-count");
      var cartPay_total_count_value = parseInt(cartPay_total_count.innerText);
      product_count += count;
      product_count_box.innerText = product_count;
      if (product_count > 1) {
        product_count_box.classList.remove("hide");
      }
      // aktualizace celkoveho poctu ks pro mobilni vzhled
      cartPay_total_count_value += count;
      cartPay_total_count.innerText = cartPay_total_count_value;


      var countBox = document.getElementById("quick-cart-product-count-" + variantId);
      countBox.classList.remove('fadeDown')
      countBox.classList.add('fadeUp')

      if (count < 0) {
        count = 1;
      }

      if (product_box) {
        product_box.classList.remove("show-remove-all");
        last_removed_variant_count = 0;
      }

      var ajax = new XMLHttpRequest();
      ajax.onreadystatechange = function() {
        if (ajax.readyState == 4 && ajax.status == 200) {
          ajaxUpdateCart();
        }
      };
      ajax.open("POST", "/cart/add.js", true);
      ajax.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
      ajax.send(JSON.stringify({quantity: count, id: variantId}));
    }

    function ajaxRemoveProduct(data) {
      var ajax = new XMLHttpRequest();

      ajax.onreadystatechange = function() {
        if (ajax.readyState == 4 && ajax.status == 200) {
          ajaxUpdateCart();
        }
      };
      ajax.open("POST", "/cart/change.js", true);
      ajax.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
      ajax.send(JSON.stringify(data));
    }

    function ajaxRemoveAllProducts(data) {
      var ajax = new XMLHttpRequest();
      ajax.onreadystatechange = function() {
        if (ajax.readyState == 4 && ajax.status == 200) {
          ajaxUpdateCart();
        }
      };
      ajax.open("POST", "/cart/change.js", true);
      ajax.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
      ajax.send(JSON.stringify(data));
    }

    // Function for updating cart with quantity of actual item and orice
    // Private function
    // Arguments:
    // Returning nothing
    function ajaxUpdateCart() {
      var ajax = new XMLHttpRequest();
      ajax.onreadystatechange = function() {
        if (ajax.readyState == 4 && ajax.status == 200) {
          var response = JSON.parse(ajax.responseText);
          cartPrice.innerText = Shopify.formatMoney(response.total_price);
          // Zobrazeni tlacitka na nakup
          if (response.total_price <= 0) {
            cartPay.classList.remove("open");
          }
        }
      };
      ajax.open("GET", "/cart.js", true);
      ajax.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
      ajax.send();
    }

    // Function for removing html of product from cart
    // Private function
    function removeProduct(product_box) {
      cart.removeChild(product_box);
    }

    // procedure for changing cart link destionation depending on size of screen.
    function setPayButtonAction() {
      if (cartPay) {
        // Cart is fixed in right bottom of screen
        var window_w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

        if (window_w > 600) {
          cartPay.setAttribute("href", "/checkout");
          cartPay.classList.remove("cart-ico");
        } else {
          cartPay.setAttribute("href", "/cart");
          cartPay.classList.add("cart-ico");
        }
      }
    }
    //public methods
    return self;
  };

  return {
    // Get the Singleton instance if one exists
    // or create one if it doesn't
    init: function (att) {
      if ( !instance ) {
        instance = init(att);
      }
      return instance;
    }
  };

})();


// Usage:
var cart = Quickbeam.init({
  'animationLib': 'gsap'
});
//# sourceURL=pen.js
</script>
    

	<script src="js/vendor/jquery-2.2.4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	 crossorigin="anonymous"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/jquery.nice-select.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/nouislider.min.js"></script>
	<script src="js/countdown.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
	<script src="instafeed.min.js"></script>
	<script src="custom.js"></script>
	
    
    
  
   
	<!--gmaps Js-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="js/gmaps.min.js"></script>
    <script src="js/main.js"></script>

  </body>
</html>
