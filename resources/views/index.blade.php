<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="no-js">

<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="CodePixar">
	<!-- Meta Description -->
	
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>Karma Shop</title>
	<!--
		CSS
        ============================================= -->
		<link rel="stylesheet" href="css/bootstrap.css">
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link rel="stylesheet" href="css/linearicons.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/themify-icons.css">
	<link rel="stylesheet" href="css/amazonmenu.css">

	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/nice-select.css">
	<link rel="stylesheet" href="css/nouislider.min.css">
	<link rel="stylesheet" href="css/ion.rangeSlider.css" />
	<link rel="stylesheet" href="css/ion.rangeSlider.skinFlat.css" />
	<link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/main.css">
	
	<link rel="stylesheet" href="css/style.css">
    <link href="assets/owl.carousel.min.css" rel="stylesheet"/>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/style3.css">
<script src="/js/tog.js"></script>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="/js/amazonmenu.js"></script>
<link rel="stylesheet" href="css/amazonmenu.css">
<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu1'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu2'
		
	})
})

</script>



<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu3'
		
	})
})

</script>


<script>

jQuery(function(){
	amazonmenu.init({
		menuid: 'mysidebarmenu4'
		
	})
})

</script>






<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body>

@include('layout.menubar')




	<!-- start banner Area -->
	<section class="banner-area" style="margin-top:80px;background-image: url('https://newevolutiondesigns.com/images/freebies/black-wallpaper-10.jpg');">
		<div class="container">
			<div class="row fullscreen align-items-center justify-content-start">
				<div class="col-lg-12">
					<div class="active-banner-slider owl-carousel">
						<!-- single-slide -->
						<div class="row single-slide align-items-center d-flex">
							<div class="col-lg-5 col-md-6">
								<div class="banner-content">
									<h1 style="color:white">Nike New <br>Collection!</h1>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
										dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
									<div class="add-bag d-flex align-items-center">
										<a class="add-btn" href=""><span class="lnr lnr-cross"></span></a>
										<span class="add-text text-uppercase" style="color:white">Add to cart</span>
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="banner-img">
									<img class="img-fluid" style="margin-top:30px;height:500px" src="/img/c2_professional_boxing_gloves_5__1_1.png" alt="">
								</div>
							</div>
						</div>
						<!-- single-slide -->
						<div class="row single-slide">
							<div class="col-lg-5">
								<div class="banner-content">
									<h1 style="color:white; margin-top:90px">Nike New <br>Collection!</h1>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
										dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
									<div class="add-bag d-flex align-items-center">
										<a class="add-btn" href=""><span class="lnr lnr-cross"></span></a>
										<span class="add-text text-uppercase" style="color:white">Add to cart</span>
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="banner-img">
									<img class="img-fluid" style="margin-top:30px;height:500px" src="/img/c2_professional_boxing_gloves_5__1_1.png" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->
	<hr style="background-color:orange" >
	<!-- Start category Area -->
	<div class="container-fluid">
    <header class="text-center">
	<div class="jumbotron jumbotron-fluid" style="background-image:url('/img/j3.jpg');background-repeat: no-repeat, repeat;background-size: cover;">
  <div class="container">
    <h1 class="display-4" style="color:whitesmoke">Our Categories</h1>
    <p class="lead">Stay tuned to the latest news & social buzz around RDX</p>
  </div>
</div>
		</header>
		<div class="container">
	<div class="row">
	  <div class="col-md-8 col-sm-12 co-xs-12 gal-item">
		   <div class="row h-50">
				  <div class="col-md-12 col-sm-12 co-xs-12 gal-item">
							<div class="box">
						 <img src="http://fakeimg.pl/758x370/" class="img-ht img-fluid rounded">
							</div>
					</div>
			</div>
	  
	    <div class="row h-50">
				 <div class="col-md-6 col-sm-6 co-xs-12 gal-item">
				  <div class="box">
					<img src="http://fakeimg.pl/748x177/" class="img-ht img-fluid rounded">
				</div>
				</div>

				<div class="col-md-6 col-sm-6 co-xs-12 gal-item">
				 <div class="box">
					<img src="http://fakeimg.pl/371x370/" class="img-ht img-fluid rounded">
				</div>
				</div>
            </div>
      </div>

           <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
			   <div class="col-md-12 col-sm-6 co-xs-12 gal-item h-25">
				<div class="box">
					<img src="http://fakeimg.pl/748x177/" class="img-ht img-fluid rounded">
				</div>
				</div>

				  <div class="col-md-12 col-sm-6 co-xs-12 gal-item h-75">
				   <div class="box">
					<img src="http://fakeimg.pl/748x177/" class="img-ht img-fluid rounded">
				</div>
				</div>
            </div>

	</div></div>
	<br/>
</div>
	<!-- End category Area -->

	<!-- start product Area -->
	<section class="owl-carousel active-product-area section_gap">
		<!-- single product slide -->
		<div class="single-product-slider">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-6 text-center">
						<div class="section-title">
							<h1 style="color:white">Latest Products</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
								dolore
								magna aliqua.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<!-- single product -->
					<div class="col-lg-3 col-md-6">
						<div class="single-product" style="background-color:grey ;padding:10px">
							<img class="img-fluid" src="https://media.rdxsports.com/catalog/product/cache/f073062f50e48eb0f0998593e568d857/1/_/1.jpg" alt="">
							<div class="product-details">
								<h6 style="color:white">addidas New Hammer sole
									for Sports person</h6>
								<div class="price">
									<h6 style="color:white">$150.00</h6>
									<h6 class="l-through">$210.00</h6>
								</div>
								<div class="prd-bottom">

									<a href="" class="social-info">
										<span class="ti-bag"></span>
										<p class="hover-text" style="color:white">add to cart</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text" style="color:white">Wishlist</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text" style="color:white">compare</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text" style="color:white">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- single product -->
					<div class="col-lg-3 col-md-6">
						<div class="single-product" style="background-color:grey ;padding:10px">
							<img class="img-fluid" src="https://media.rdxsports.com/catalog/product/cache/f073062f50e48eb0f0998593e568d857/t/6/t6_mma_grappling_gloves_blue_4__3.jpg" alt="">
							<div class="product-details">
								<h6 style="color:white">addidas New Hammer sole
									for Sports person</h6>
								<div class="price">
									<h6 style="color:white">$150.00</h6>
									<h6 class="l-through">$210.00</h6>
								</div>
								<div class="prd-bottom">

									<a href="" class="social-info">
										<span class="ti-bag"></span>
										<p class="hover-text" style="color:white">add to cart</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text" style="color:white">Wishlist</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text" style="color:white">compare</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text" style="color:white">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- single product -->
					<div class="col-lg-3 col-md-6">
						<div class="single-product" style="background-color:grey ;padding:10px">
							<img class="img-fluid" src="https://media.rdxsports.com/catalog/product/cache/f073062f50e48eb0f0998593e568d857/t/1/t15_noir_black_focus_pads_1__1.jpg" alt="">
							<div class="product-details">
								<h6 style="color:white">addidas New Hammer sole
									for Sports person</h6>
								<div class="price">
									<h6 style="color:white">$150.00</h6>
									<h6 class="l-through">$210.00</h6>
								</div>
								<div class="prd-bottom">
									<a href="" class="social-info">
										<span class="ti-bag"></span>
										<p class="hover-text" style="color:white">add to cart</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text" style="color:white">Wishlist</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text" style="color:white">compare</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text" style="color:white">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- single product -->
					<div class="col-lg-3 col-md-6">
						<div class="single-product" style="background-color:grey ;padding:10px">
							<img class="img-fluid" src="https://media.rdxsports.com/catalog/product/cache/f073062f50e48eb0f0998593e568d857/s/3/s3_bjj_gi_2_.jpg" alt="">
							<div class="product-details">
								<h6 style="color:white" >addidas New Hammer sole
									for Sports person</h6>
								<div class="price">
									<h6 style="color:white">$150.00</h6>
									<h6 class="l-through">$210.00</h6>
								</div>
								<div class="prd-bottom">

									<a href="" class="social-info">
										<span class="ti-bag"></span>
										<p class="hover-text" style="color:white">add to cart</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text" style="color:white">Wishlist</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text" style="color:white">compare</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text" style="color:white">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- single product -->
					
					<!-- single product -->
				
					<!-- single product -->
					
					<!-- single product -->
					
				</div>
			</div>
		</div>
		<!-- single product slide -->
		<div class="single-product-slider">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-6 text-center">
						<div class="section-title">
							<h1 style="color:white">Coming Products</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
								dolore
								magna aliqua.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<!-- single product -->
					<div class="col-lg-3 col-md-6">
						<div class="single-product" style="background-color:grey ;padding:10px">
							<img class="img-fluid" src="https://media.rdxsports.com/catalog/product/cache/f073062f50e48eb0f0998593e568d857/t/6/t6_mma_grappling_gloves_blue_4__3.jpg" alt="">
							<div class="product-details">
								<h6 style="color:white">addidas New Hammer sole
									for Sports person</h6>
								<div class="price">
									<h6 style="color:white">$150.00</h6>
									<h6 class="l-through">$210.00</h6>
								</div>
								<div class="prd-bottom">

									<a href="" class="social-info">
										<span class="ti-bag"></span>
										<p class="hover-text" style="color:white">add to cart</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text" style="color:white">Wishlist</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text" style="color:white">compare</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text" style="color:white">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- single product -->
					<div class="col-lg-3 col-md-6">
						<div class="single-product" style="background-color:grey ;padding:10px">
							<img class="img-fluid" src="https://media.rdxsports.com/catalog/product/cache/f073062f50e48eb0f0998593e568d857/t/6/t6_mma_grappling_gloves_blue_4__3.jpg" alt="">
							<div class="product-details">
								<h6 style="color:white">addidas New Hammer sole
									for Sports person</h6>
								<div class="price">
									<h6 style="color:white">$150.00</h6>
									<h6 class="l-through">$210.00</h6>
								</div>
								<div class="prd-bottom">

									<a href="" class="social-info">
										<span class="ti-bag"></span>
										<p class="hover-text" style="color:white">add to cart</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text" style="color:white">Wishlist</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text" style="color:white">compare</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text" style="color:white">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- single product -->
					<div class="col-lg-3 col-md-6">
						<div class="single-product" style="background-color:grey ;padding:10px">
							<img class="img-fluid" src="https://media.rdxsports.com/catalog/product/cache/f073062f50e48eb0f0998593e568d857/t/6/t6_mma_grappling_gloves_blue_4__3.jpg" alt="">
							<div class="product-details">
								<h6 style="color:white">addidas New Hammer sole
									for Sports person</h6>
								<div class="price">
									<h6 style="color:white">$150.00</h6>
									<h6 class="l-through">$210.00</h6>
								</div>
								<div class="prd-bottom">

									<a href="" class="social-info">
										<span class="ti-bag"></span>
										<p class="hover-text" style="color:white">add to cart</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text" style="color:white">Wishlist</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text" style="color:white">compare</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text" style="color:white">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- single product -->
					<div class="col-lg-3 col-md-6">
						<div class="single-product" style="background-color:grey ;padding:10px">
							<img class="img-fluid" src="https://media.rdxsports.com/catalog/product/cache/f073062f50e48eb0f0998593e568d857/t/6/t6_mma_grappling_gloves_blue_4__3.jpg" alt="">
							<div class="product-details">
								<h6 style="color:white">addidas New Hammer sole
									for Sports person</h6>
								<div class="price">
									<h6 style="color:white">$150.00</h6>
									<h6 class="l-through">$210.00</h6>
								</div>
								<div class="prd-bottom">

									<a href="" class="social-info">
										<span class="ti-bag"></span>
										<p class="hover-text" style="color:white">add to cart</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text" style="color:white">Wishlist</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text" style="color:white">compare</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text" style="color:white">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- single product -->
				
					<!-- single product -->
					
					<!-- single product -->
				
					<!-- single product -->
					
				</div>
			</div>
		</div>
	</section>
	<!-- end product Area -->

	<!-- start product Area -->
	<section class="owl-carousel active-product-area">
		<!-- single product slide -->
		<div class="single-product-slider">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-6 text-center">
						<div class="section-title">
							<h1 style="color:white">Latest Products</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
								dolore
								magna aliqua.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<!-- single product -->
					<div class="col-lg-3 col-md-6">
						<div class="single-product" style="background-color:grey ;padding:10px">
							<img class="img-fluid" src="https://media.rdxsports.com/catalog/product/cache/f073062f50e48eb0f0998593e568d857/1/_/1.jpg" alt="">
							<div class="product-details">
								<h6 style="color:white">addidas New Hammer sole
									for Sports person</h6>
								<div class="price">
									<h6 style="color:white">$150.00</h6>
									<h6 class="l-through">$210.00</h6>
								</div>
								<div class="prd-bottom">

									<a href="" class="social-info">
										<span class="ti-bag"></span>
										<p class="hover-text" style="color:white">add to cart</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text" style="color:white">Wishlist</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text" style="color:white">compare</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text" style="color:white">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- single product -->
					<div class="col-lg-3 col-md-6">
						<div class="single-product" style="background-color:grey ;padding:10px">
							<img class="img-fluid" src="https://media.rdxsports.com/catalog/product/cache/f073062f50e48eb0f0998593e568d857/t/6/t6_mma_grappling_gloves_blue_4__3.jpg" alt="">
							<div class="product-details">
								<h6 style="color:white">addidas New Hammer sole
									for Sports person</h6>
								<div class="price">
									<h6 style="color:white">$150.00</h6>
									<h6 class="l-through">$210.00</h6>
								</div>
								<div class="prd-bottom">

									<a href="" class="social-info">
										<span class="ti-bag"></span>
										<p class="hover-text" style="color:white">add to cart</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text" style="color:white">Wishlist</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text" style="color:white">compare</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text" style="color:white">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- single product -->
					<div class="col-lg-3 col-md-6">
						<div class="single-product" style="background-color:grey ;padding:10px">
							<img class="img-fluid" src="https://media.rdxsports.com/catalog/product/cache/f073062f50e48eb0f0998593e568d857/t/1/t15_noir_black_focus_pads_1__1.jpg" alt="">
							<div class="product-details">
								<h6 style="color:white">addidas New Hammer sole
									for Sports person</h6>
								<div class="price">
									<h6 style="color:white">$150.00</h6>
									<h6 class="l-through">$210.00</h6>
								</div>
								<div class="prd-bottom">
									<a href="" class="social-info">
										<span class="ti-bag"></span>
										<p class="hover-text" style="color:white">add to cart</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text" style="color:white">Wishlist</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text" style="color:white">compare</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text" style="color:white">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- single product -->
					<div class="col-lg-3 col-md-6">
						<div class="single-product" style="background-color:grey ;padding:10px">
							<img class="img-fluid" src="https://media.rdxsports.com/catalog/product/cache/f073062f50e48eb0f0998593e568d857/s/3/s3_bjj_gi_2_.jpg" alt="">
							<div class="product-details">
								<h6 style="color:white" >addidas New Hammer sole
									for Sports person</h6>
								<div class="price">
									<h6 style="color:white">$150.00</h6>
									<h6 class="l-through">$210.00</h6>
								</div>
								<div class="prd-bottom">

									<a href="" class="social-info">
										<span class="ti-bag"></span>
										<p class="hover-text" style="color:white">add to cart</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text" style="color:white">Wishlist</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text" style="color:white">compare</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text" style="color:white">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- single product -->
					
					<!-- single product -->
				
					<!-- single product -->
					
					<!-- single product -->
					
				</div>
			</div>
		</div>
		<!-- single product slide -->
		<div class="single-product-slider">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-6 text-center">
						<div class="section-title">
							<h1 style="color:white">Coming Products</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
								dolore
								magna aliqua.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<!-- single product -->
					<div class="col-lg-3 col-md-6">
						<div class="single-product" style="background-color:grey ;padding:10px">
							<img class="img-fluid" src="https://media.rdxsports.com/catalog/product/cache/f073062f50e48eb0f0998593e568d857/t/6/t6_mma_grappling_gloves_blue_4__3.jpg" alt="">
							<div class="product-details">
								<h6 style="color:white">addidas New Hammer sole
									for Sports person</h6>
								<div class="price">
									<h6 style="color:white">$150.00</h6>
									<h6 class="l-through">$210.00</h6>
								</div>
								<div class="prd-bottom">

									<a href="" class="social-info">
										<span class="ti-bag"></span>
										<p class="hover-text" style="color:white">add to cart</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text" style="color:white">Wishlist</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text" style="color:white">compare</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text" style="color:white">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- single product -->
					<div class="col-lg-3 col-md-6">
						<div class="single-product" style="background-color:grey ;padding:10px">
							<img class="img-fluid" src="https://media.rdxsports.com/catalog/product/cache/f073062f50e48eb0f0998593e568d857/t/6/t6_mma_grappling_gloves_blue_4__3.jpg" alt="">
							<div class="product-details">
								<h6 style="color:white">addidas New Hammer sole
									for Sports person</h6>
								<div class="price">
									<h6 style="color:white">$150.00</h6>
									<h6 class="l-through">$210.00</h6>
								</div>
								<div class="prd-bottom">

									<a href="" class="social-info">
										<span class="ti-bag"></span>
										<p class="hover-text" style="color:white">add to cart</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text" style="color:white">Wishlist</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text" style="color:white">compare</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text" style="color:white">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- single product -->
					<div class="col-lg-3 col-md-6">
						<div class="single-product" style="background-color:grey ;padding:10px">
							<img class="img-fluid" src="https://media.rdxsports.com/catalog/product/cache/f073062f50e48eb0f0998593e568d857/t/6/t6_mma_grappling_gloves_blue_4__3.jpg" alt="">
							<div class="product-details">
								<h6 style="color:white">addidas New Hammer sole
									for Sports person</h6>
								<div class="price">
									<h6 style="color:white">$150.00</h6>
									<h6 class="l-through">$210.00</h6>
								</div>
								<div class="prd-bottom">

									<a href="" class="social-info">
										<span class="ti-bag"></span>
										<p class="hover-text" style="color:white">add to cart</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text" style="color:white">Wishlist</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text" style="color:white">compare</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text" style="color:white">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- single product -->
					<div class="col-lg-3 col-md-6">
						<div class="single-product" style="background-color:grey ;padding:10px">
							<img class="img-fluid" src="https://media.rdxsports.com/catalog/product/cache/f073062f50e48eb0f0998593e568d857/t/6/t6_mma_grappling_gloves_blue_4__3.jpg" alt="">
							<div class="product-details">
								<h6 style="color:white">addidas New Hammer sole
									for Sports person</h6>
								<div class="price">
									<h6 style="color:white">$150.00</h6>
									<h6 class="l-through">$210.00</h6>
								</div>
								<div class="prd-bottom">

									<a href="" class="social-info">
										<span class="ti-bag"></span>
										<p class="hover-text" style="color:white">add to cart</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text" style="color:white">Wishlist</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text" style="color:white">compare</p>
									</a>
									<a href="" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text" style="color:white">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- single product -->
				
					<!-- single product -->
					
					<!-- single product -->
				
					<!-- single product -->
					
				</div>
			</div>
		</div>
	</section>
	<!-- end product Area -->




<!-- start instagram feee Area -->


<div class="jumbotron jumbotron-fluid" style="background-image:url('/img/j3.jpg');background-repeat: no-repeat, repeat;background-size: cover;">
  <div class="container">
    <h1 class="display-4" style="color:whitesmoke;text-align:center">RDX Sports Buzz</h1>
    <p class="lead" style="text-align:center">Stay tuned to the latest news & social buzz around RDX</p>
  </div>
</div>


<div class="sqs-block-content">

<!-- LightWidget WIDGET --><script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/aed5b92353e0548bbedc6a11a9b49271.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>

</div>



<!-- end instagram feed Area -->


<!-- Emaill Newsletter-->

<div class="container">
		<h1 class="text-center m-5" style="color:white">SUBSCRIBE TO OUR NEWSLETTER</h1>
		
	</div>
	<section class="bg-dark text-center p-5 mt-4">
		<div class="container p-3">
			<h3 class="text-white">RECEIVE GEAR RECOMMENDATIONS</h3>
			<p class="text-white">Join the RDX Sports Members Club to be the first to know about new product releases & promotions.</p>
			<form action="#" method="Post">
			<div class="container">
				<input class="form-control" type="text" name="text" placeholder="Enter Your Email Id">
				<button type="button" class="btn btn-danger" style="margin-top:20px; ">Subscribe <i class="fa fa-envelope"></i></button>
</div>
			</form>
		</div>
	</section>
	

<!-- Emaill Newsletter-->


@include('layout.Footer')
    
    

	<script src="js/vendor/jquery-2.2.4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	 crossorigin="anonymous"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/jquery.nice-select.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/nouislider.min.js"></script>
	<script src="js/countdown.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
	<script src="instafeed.min.js"></script>
	<script src="custom.js"></script>
	
    
    
  
   
	<!--gmaps Js-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="js/gmaps.min.js"></script>
    <script src="js/main.js"></script>
    
   
    
</body>

</html>